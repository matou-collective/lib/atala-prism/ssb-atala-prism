# Credential Issuance

## Issuance Process (With Connection)

```mermaid
sequenceDiagram
  autonumber
  
  actor Peer as Tribal Member
  actor Kaitiaki as Tribal Kaitiaki
  participant Issuer
  
  Peer -->> Kaitiaki: Here is my registration

  Kaitiaki -->> Peer: add-member
  
  rect rgb(255, 255, 255)
    note over Peer, Issuer: Establish Connection 

    Kaitiaki ->> +Issuer: request connection invitation (OOB)
    Issuer ->> -Kaitiaki: connection invitation (OOB)

    Kaitiaki -->> Peer: CRED_CONN_INVITE_SENT

    Peer ->> +Issuer: accept connection invitation
    Issuer ->> -Peer: OK
  
    Peer -->> Kaitiaki: CRED_CONN_COMPLETE
  end
  
  rect rgb(255, 255, 255)
    note over Peer, Issuer: Issue Credential
    
    Kaitiaki ->> +Issuer: make credential offer
    Issuer ->> -Kaitiaki: OK

    Kaitiaki -->> Peer: CRED_OFFER_SENT
    
    Issuer ->> Peer: Credential Offer
    Peer ->> Issuer: accept credential offer
    Issuer ->> Peer: Verifiable Credential
  
    Peer -->> Kaitiaki: CRED_COMPLETE
  end
  
```

### Steps

**Registration Steps:**

1. The `Tribal Member` sends a registration to join the `Tribe`, which is sent to the `Tribal Kaitiaki`
2. The `Tribal Kaitiaki` accepts the registration, the member is added to the `Tribe` and begins the VC issuance process

**Establishing a connection between the `Tribal Member` and the `Issuer`:**

3. The `Tribal Kaitiaki` sends the request to the `Issuer` for an `OOB connection invitation`
4. The `Issuer` responds with the `OOB connection invitation`
5. The `Tribal Kaitiaki` receives the response and updates the status of the record to `CRED_CONN_INVITE_SENT`
6. The `Tribal Member` sees the `CRED_CONN_INVITE_SENT` status and accepts the invitation
7. The `Issuer` responds that the connection is complete
8. The `Tribal Member`updates the status of the record to `CRED_CONN_COMPLETE`
9. The `Tribal Kaitiaki` receives the `CRED_CONN_COMPLETE` status update and starts the credential offer process, by sending the request to the `Issuer` to make the credential offer
10. The `Issuer` responds that it will make the credential offer
11. The `Tribal Kaitiaki` updates the status of the record to `CRED_OFFER_SENT`
12. The `Issuer`sends the `Credential Offer` to the `Tribal Member`
13. The Tribal Member reads the `CRED_OFFER_SENT`update change and listens for the credential offer. Once they receive it, they let the `Issuer` know they accept it
14. The `Issuer` then sends the `Verifiable Credential` to the `Tribal Member`
15. The `Tribal Member` receives the `Verifiable Credential` and then marks the record status as `CRED_COMPLETE`


## Issuance Process (Connectionless)

```mermaid
sequenceDiagram
  autonumber
  
  actor Peer as Tribal Member
  actor Kaitiaki as Tribal Kaitiaki
  participant Issuer
  
  Peer -->> Kaitiaki: here is my registration
  
  Kaitiaki -->> Peer: add-member
  
  rect rgb(255, 255, 255)
    note over Peer, Issuer: Set up Credential Offer
    Kaitiaki->> +Issuer: request VC issuance invitation (OOB)
    Issuer->> -Kaitiaki: VC issuance invitation

    Kaitiaki-->>Peer: CRED_INVITE_SENT
  end
  
  

  rect rgb(255, 255, 255)
    note over Peer, Issuer: Issue Credential
    Peer ->> +Issuer: accept VC issuance invitation
    Issuer ->> -Peer: OK
    Peer -->> Kaitiaki: CRED_OFFER_SENT
    Issuer ->> Peer: Credential Offer
    Peer ->> Issuer: accept credential offer
    Issuer ->> Peer: Verifiable Credential
    Peer -->> Kaitiaki: CRED_COMPLETE
  end
```

**Registration Steps:**

1. The `Tribal Member` sends a registration to join the `Tribe`, which is sent to the `Tribal Kaitiaki`
2. The `Tribal Kaitiaki` accepts the registration, the member is added to the `Tribe` and begins the VC issuance process

**Setting up a connectionless credential offer between the `Tribal Member` and the `Issuer`:**

3. The `Tribal Kaitiaki` sends the request to the `Issuer` for an `VC issuance invitation`
4. The `Issuer` responds with the `VC issuance invitation`
5. The `Tribal Kaitiaki` receives the response and updates the status of the record to `CRED_INVITE_SENT`
6. The `Tribal Member` sees the `CRED_INVITE_SENT` status and accepts the invitation
7. The `Issuer` responds that the invitation has been accepted, and will proceed with the credential offer
8. The `Tribal Member`updates the status of the record to `CRED_OFFER_SENT`
9. The `Issuer`sends the `Credential Offer` to the `Tribal Member`
10. The Tribal Member reads the `CRED_OFFER_SENT`update change and listens for the credential offer. Once they receive it, they let the `Issuer` know they accept it
11. The `Issuer` then sends the `Verifiable Credential` to the `Tribal Member`
12. The `Tribal Member` receives the `Verifiable Credential` and then marks the record status as `CRED_COMPLETE`

## States

### Issuance Process (With Connection)

```mermaid
sequenceDiagram
  autonumber

  actor Peer as Tribal Member
  actor Kaitiaki as Tribal Kaitiaki

  Kaitiaki -->> Peer: CRED_CONN_INVITE_SENT
  Peer -->> Kaitiaki: CRED_CONN_COMPLETE
  Kaitiaki -->> Peer: CRED_OFFER_SENT
  Peer -->> Kaitiaki: CRED_COMPLETE
```

### Issuance Process (Connectionless)

```mermaid
sequenceDiagram
  autonumber

  actor Peer as Tribal Member
  actor Kaitiaki as Tribal Kaitiaki

  Kaitiaki-->>Peer: CRED_INVITE_SENT
  Peer -->> Kaitiaki: CRED_OFFER_SENT
  Peer -->> Kaitiaki: CRED_COMPLETE
```


These states help track the credential presentation and verification process, ensuring clear communication between the `Tribal Kaitiaki`, `Tribal Member`, and the `Verifier`.

- **`CRED_CONN_INVITE_SENT`**
  - This state indicates that the `Tribal Kaitiaki` has sent a connection invitation to the `Issuer` to establish a connection for the credential issuance process. The invitation is necessary for the subsequent communication between the `Tribal Member` and the `Issuer`.
- **`CRED_CONN_COMPLETE`**
  - This state signifies that the Tribal Member has accepted the connection invitation. The connection establishment is now complete, allowing the `Tribal Kaitiaki` and `Issuer` to proceed with the credential issuance process.
- **`CRED_INVITE_SENT`**
  - This state represents the completion of the process where the `Tribal Kaitiaki` sends an invitation to the `Issuer` for VC issuance. This is part of the connectionless issuance process, indicating that the `Tribal Member` can now accept this invitation.
- **`CRED_OFFER_SENT`**
  - In this state, the `Tribal Kaitiaki` has successfully made a credential offer request to the `Issuer`, and the `Issuer` has acknowledged this request. This state serves as a notification that the offer process has started.
- **`CRED_COMPLETE`**
  - This state indicates that the `Tribal Member` has received the `Verifiable Credential` from the `Issuer` and has completed the credential issuance process. It marks the successful end of the process, confirming that the member is now recognized with the credential.
