# Credential Presentation and Verification

## Presentation and Verification Process (With Connection)

```mermaid
sequenceDiagram
  autonumber
  
  actor Peer as Tribal Member
  actor Kaitiaki as Tribal Kaitiaki
  participant Verifier
  
  Peer-->>  Kaitiaki: here is my registration

  rect rgb(255, 255, 255)
    note over Peer, Verifier: Establish Connection 

    Kaitiaki ->> +Verifier: request connection invitation (OOB)
    Verifier ->> -Kaitiaki: connection invitation (OOB)

    Kaitiaki -->> Peer: PRES_CONN_INVITE_SENT

    Peer ->> +Verifier: accept connection invitation
    Verifier ->> -Peer: OK

    Peer -->> Kaitiaki: PRES_CONN_COMPLETE
  end
  
  
  rect rgb(255, 255, 255)
    note over Peer, Verifier: Present Credential
    
    Kaitiaki ->> +Verifier: make presentation request
    Verifier ->> -Kaitiaki: OK

    Kaitiaki -->> Peer: PRES_REQUEST_SENT
    
    Verifier ->> Peer: Presentation Request
    Peer ->> Verifier: Presentation
    Peer -->> Kaitiaki: PRES_SENT
    Kaitiaki ->> +Verifier: get presentation
    Verifier ->> -Kaitiaki: Verified

    Kaitiaki -->> Peer: PRES_COMPLETE
  end
  
  
  Kaitiaki -->> Peer: add-member

```

### Steps

**Registration Steps:**

1. The `Tribal Member` sends a registration to the `Tribal Kaitiaki` to initiate the verification process.

**Establishing a connection between the `Tribal Member` and the `Verifier`:**

2. The `Tribal Kaitiaki` sends a request to the `Verifier`for an `OOB connection invitation`
3. The `Verifier` responds with the `OOB connection invitation`
4. The `Tribal Kaitiaki` receives the response and updates the status of the record to `PRES_CONN_INVITE_SENT`
5. The `Tribal Member` sees the `PRES_CONN_INVITE_SENT` status and accepts the invitation
6. The `Verifier` responds that the connection is complete
7. The `Tribal Member`updates the status of the record to `PRES_CONN_COMPLETE`

**Credential Presentation:**

8. The `Tribal Kaitiaki` receives the `CRED_CONN_COMPLETE` status update and starts the credential offer process, by sending the request to the `Verifier` to make the presentation request
9. The `Verifier` responds that it will make the presentation request
10. The `Tribal Kaitiaki` updates the status of the record to `PRES_REQUEST_SENT`
11. The `Verifier`sends the `Presentation Request` to the `Tribal Member`
12. The `Tribal Member` reads the `PRES_REQUEST_SENT`update change and listens for the request to respond to
13. The `Tribal Member` has sent the `Presentation` and marks the record status as `PRES_SENT`
14. The `Tribal Kaitiaki` sees the status `PRES_SENT` and sends the request to the `Verifier`about the presentation
15. The `Verifier`responds that the `Presentation` was `Verified`
16. The `Tribal Kaitiaki` marks the status of the record as `PRES_COMPLETE`

**Registration Steps:**

17. The `Tribal Member` is added to the `Tribe`

## Presentation and Verification Process (Connectionless)

```mermaid
sequenceDiagram
  autonumber
  
  actor Peer as Tribal Member
  actor Kaitiaki as Tribal Kaitiaki
  participant Verifier
  
  Peer -->> Kaitiaki: here is my registration
  
  rect rgb(255, 255, 255) 
    note over Peer, Verifier: Setup Holder Presentation 
    
    Kaitiaki->> +Verifier: make VC presentation invitation (OOB)
    Verifier->> -Kaitiaki: VC presentation invitation
    Kaitiaki-->>Peer: PRES_INVITE_SENT
  end

  rect rgb(255, 255, 255)
    note over Peer, Verifier: Present Credential
    
    Peer ->> +Verifier: accept VC presentation invitation
    Verifier ->> -Peer: OK
    Verifier ->> Peer: Presentation Request
    Peer ->> Verifier: Presentation
    Peer -->> Kaitiaki: PRES_SENT
    Kaitiaki ->> +Verifier: get presentation
    Verifier ->> -Kaitiaki: Verified
  
    Kaitiaki -->> Peer: PRES_COMPLETE
  end
  
  
  Kaitiaki -->> Peer: add-member
```

### Steps

**Registration Steps:**

1. The `Tribal Member` submits their registration to the `Tribal Kaitiaki`.

**Setting up a connectionless presentation request between the `Tribal Member` and the `Verifier`:**

2. The `Tribal Kaitiaki` sends the request to the `Verifier` for an `VC presentation invitation`
3. The `Verifier` responds with the `VC presentation invitation`
4. The `Tribal Kaitiaki` receives the response and updates the status of the record to `PRES_INVITE_SENT`

**Credential Presentation:**
5. The `Tribal Member` sees the `PRES_INVITE_SENT` status and accepts the invitation
6. The `Verifier` responds that the invitation has been accepted, and will proceed with the presentation request
7. The `Verifier` sends the presentation request to the `Tribal Member`.
8. The `Tribal Member` presents their credentials to the `Verifier`.
9. The `Tribal Member` updates the status of the record to `PRES_SENT`.
10. The `Tribal Kaitiaki` retrieves the presentation from the `Verifier`.
11. The `Verifier` confirms the credential verification
12. The `Tribal Kaitiaki` updates the status to `PRES_COMPLETE` and adds the `Tribal Member` to the tribe.

**Registration Steps:**

13. The `Tribal Member`is added to the `Tribe`

## States

### Presentation and Verification Process (With Connection)

```mermaid
sequenceDiagram
  autonumber
  
  Kaitiaki -->> Peer: PRES_CONN_INVITE_SENT
  Peer -->> Kaitiaki: PRES_CONN_COMPLETE
  Kaitiaki -->> Peer: PRES_REQUEST_SENT
  Peer -->> Kaitiaki: PRES_SENT
  Kaitiaki -->> Peer: PRES_COMPLETE

```

### Presentation and Verification Process (Connectionless)

```mermaid
sequenceDiagram
  autonumber
  
  Kaitiaki-->>Peer: PRES_INVITE_SENT
  Peer -->> Kaitiaki: PRES_SENT
  Kaitiaki -->> Peer: PRES_COMPLETE
```

These states help track the credential presentation and verification process, ensuring clear communication between the `Tribal Kaitiaki`, `Tribal Member`, and the `Verifier`.

- **`PRES_CONN_INVITE_SENT`**
  - This state indicates that the `Tribal Kaitiaki` has forwarded a connection invitation to the `Tribal Member` from the `Verifier`, allowing them to establish a connection for the presentation process.
- **`PRES_CONN_COMPLETE`**
  - This state signifies that the `Tribal Member` has accepted the connection invitation, completing the connection between them and the `Verifier`.
- **`PRES_INVITE_SENT`**
  - This state represents the completion of the process where the `Tribal Kaitiaki` sends an invitation to the `Verifier` for VC presentation. This is part of the connectionless presentation process, indicating that the `Tribal Member` can now accept this invitation.
- **`PRES_REQUEST_SENT`**
  - This state shows that the `Tribal Kaitiaki` has sent a presentation request to the `Tribal Member`, and the `Verifier` has acknowledged it.
- **`PRES_SENT`**
  - This state indicates that the `Tribal Member` has sent their credential presentation to the `Verifier`.
- **`PRES_COMPLETE`**
  - This state represents the successful verification of the `Tribal Member`'s credentials and marks the end of the process.
