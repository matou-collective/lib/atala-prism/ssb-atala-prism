class Issuer {
  constructor (issuerConfig) {
    const { ISSUER_URL, ISSUER_APIKEY } = issuerConfig
    this.ISSUER_APIKEY = ISSUER_APIKEY
    this.ISSUER_URL = ISSUER_URL
    this.ISSUER_VERSION = '1.39.0'
  }

  // Instance method to perform health check
  async healthCheck () {
    const URL = `${this.ISSUER_URL}/_system/health`

    try {
      const response = await fetch(URL, {
        method: 'GET',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json'
        }
      })

      const json = await response.json()
      if (!json?.version) throw new Error('Issuer health check failed')
      // if (json?.version !== this.ISSUER_VERSION) {
      //   throw new Error(`Issuer Version: Your Verifier version is not support v${json.version}. Ahau currently supports v${this.ISSUER_VERSION}`)
      // }
    } catch (error) {
      if (error.message.match(/fetch failed/)) {
        throw new Error(`Issuer URL: Your issuer URL failed to fetch at ${this.ISSUER_URL}. check ${URL}`)
      } else throw error
    }
  }

  // Static async method for initialization
  static async create (verifierConfig) {
    const instance = new Issuer(verifierConfig)
    await instance.healthCheck()
    return instance
  }

  async createConnectionInvitation (opts) {
    try {
      const response = await fetch(`${this.ISSUER_URL}/connections`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          apiKey: this.ISSUER_APIKEY
        },
        body: JSON.stringify({
          label: 'Connect Issuer and Member',
          ...opts
        })
      })

      const json = await response.json()

      if (json.status === 403) throw new Error(`Issuer returned an error: ${json.detail}`)
      if (!json.invitation) throw new Error('Issuer didnt return an invitation', json)

      return json.invitation
    } catch (error) {
      console.error('Issuer: failed to create connection invitation')
      console.error(error)
      throw error
    }
  }

  // https://hyperledger.github.io/identus-docs/agent-api/#tag/Issue-Credentials-Protocol/operation/createCredentialOfferInvitation
  async requestIssuanceInvitation (didPrism, claims) {
    try {
      const response = await fetch(`${this.ISSUER_URL}/issue-credentials/credential-offers/invitation`, {
        method: 'POST',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
          apiKey: this.ISSUER_APIKEY
        },
        body: JSON.stringify({
          claims,

          goalCode: 'issue-vc',
          goal: 'Request issuance',
          credentialFormat: 'JWT',
          issuingDID: didPrism,
          automaticIssuance: true
        })
      })

      const json = await response.json()

      return json
    } catch (error) {
      console.error('Failed to get a presentation invitation', error)
      throw error
    }
  }

  async createPrismDID () {
    try {
      console.log('no dids found. Creating one...')
      const response = await fetch(`${this.ISSUER_URL}/did-registrar/dids`, {
        method: 'POST',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
          apiKey: this.ISSUER_APIKEY
        },
        body: JSON.stringify({
          documentTemplate: {
            publicKeys: [
              {
                id: 'auth-1',
                purpose: 'authentication'
              },
              {
                id: 'assert-1',
                purpose: 'assertionMethod'
              }
            ],
            services: []
          }
        })
      })

      const json = await response.json()

      return json.longFormDid
    } catch (error) {
      console.error('Failed to create prism did:', error)
      throw error
    }
  }

  async getPrismDID () {
    // get issuers PrismDID
    try {
      const response = await fetch(`${this.ISSUER_URL}/did-registrar/dids`, {
        method: 'GET',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
          apiKey: this.ISSUER_APIKEY
        }
      })
      const json = await response.json()
      return json.contents[0]?.longFormDid ?? await this.createPrismDID()
    } catch (error) {
      console.error('Failed to get prism did: ', error)
      throw error
    }
  }

  async issueCredential (didPrism, connectionId, claims) {
    try {
      const response = await fetch(`${this.ISSUER_URL}/issue-credentials/credential-offers`, {
        method: 'POST',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
          apiKey: this.ISSUER_APIKEY
        },
        body: JSON.stringify({
          claims,
          issuingDID: didPrism,
          connectionId,
          automaticIssuance: true
        })
      })

      const json = await response.json()
      return json
    } catch (error) {
      console.error('Failed to issue credential: ', error)
      throw error
    }
  }
}

module.exports = Issuer
