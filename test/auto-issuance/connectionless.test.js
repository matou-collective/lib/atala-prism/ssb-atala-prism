const { test, p, replicate, Testbot, Run, makeBreakpoint } = require('../helpers')

const pull = require('pull-stream')
const CRUT = require('ssb-crut')

const autoIssuanceSpec = require('../../spec/auto-issuance')
const { CRED_COMPLETE } = autoIssuanceSpec.states

test('auto-issuance (connectionless)', async t => {
  const run = Run(t)

  const rand = Math.round(9999 * Math.random())
  const newMemberName = 'new-member-' + rand
  const kaitiakiName = 'kaitiaiki-' + rand

  const { ssb: member } = await Testbot({ name: newMemberName })

  // <SETUP> kaitiaki (issuer) =============================================== //
  let { ssb: kaitiaki } = await Testbot({ name: kaitiakiName })

  const tribeName = 'Whangaroa Papa Hapu'
  const { groupId: tribeId, poBoxId } = await p(kaitiaki.tribes.create)({ addPOBox: true })

  // NOTE we have to restart the kaitiaki to install the issuer config
  const keys = kaitiaki.keys
  await p(kaitiaki.close)(true)
  const { ssb: kaitiakiAgain } = await Testbot({
    name: kaitiakiName,
    keys,
    rimraf: false,
    atalaPrism: {
      issuers: {
        [tribeId]: {
          tribeName,
          ISSUER_URL: process.env.ISSUER_URL,
          ISSUER_APIKEY: process.env.ISSUER_APIKEY
        }
      }
    }
  })
  kaitiaki = kaitiakiAgain
  // <SETUP> kaitiaki (issuer) =============================================== //

  await Promise.all([
    run('member agent starts', member.atalaPrism.start()),
    run('kaitiaki agent starts', kaitiaki.atalaPrism.start())
  ])

  const memberCrut = new CRUT(member, autoIssuanceSpec)
  const kaitiakiCrut = new CRUT(kaitiaki, autoIssuanceSpec)

  // this breakpoint is something we can "await"
  // it will only by "DONE" when the breakpoint.resolve is called
  const breakpoint = makeBreakpoint()
  pull(
    kaitiaki.messagesByType({
      type: autoIssuanceSpec.type,
      private: true,
      live: true
    }),
    pull.filter(m => !m.sync),
    pull.filter(m => {
      const state = m.value.content?.state?.set
      return state === CRED_COMPLETE
    }),
    pull.drain(breakpoint.resolve)
    // this says "we will be DONE with this breakpoint, when we
    // see our first CRED_COMPLETE state
  )

  const claims = {
    person: {
      fullName: 'Ben Tairea',
      dateOfBirth: '1987/XX/XX'
    }
  }

  // WARNING: KEEP THIS IN
  // we are testing against what happens in Ahau when a person is added
  // to a group AND issues a credential at the same time. We were seeing
  // a bug where this would trigger a rebuild, which would trigger
  // multiple responses in auto-respond.js, leading to forked state
  await run(
    'add member to group',
    p(kaitiaki.tribes.invite)(tribeId, [member.id], {})
  )

  // 1. The kaitiaki cretes the issuance oob for the member
  // 2. this will get a connectionless invitation that the member will receive
  // 3. The member will receive the invitation and accept it
  // 4. The issuer will issue the credential
  const recordId = await run(
    'otherKaitiaki request issuance OOB',
    kaitiaki.atalaPrism.requestIssuanceInvitation(
      tribeId,
      poBoxId,
      member.id,
      claims
    )
  )

  replicate({ from: kaitiaki, to: member, live: true, log: false })
  replicate({ from: member, to: kaitiaki, live: true, log: false })

  console.log('waiting at breakpoint...')
  console.time('time waiting')
  await breakpoint
  console.timeEnd('time waiting')

  // expect the state should have been updated to CRED_CONN_COMPLETE
  t.equal(
    await kaitiakiCrut.read(recordId).then(record => record.state),
    CRED_COMPLETE,
    'kaitiaki sees CRED_COMPLETE'
  )

  t.equal(
    await memberCrut.read(recordId).then(record => record.state),
    CRED_COMPLETE,
    'member sees CRED_COMPLETE'
  )

  const credentials = await run(
    'member gets verifiableCredentials',
    member.atalaPrism.verifiableCredentials()
  )

  // NOTE: this has since changed from auto-issuance test
  const vc = credentials[0].properties.get('vc')

  t.true(
    vc.credentialSubject?.memberOf.tribeName === tribeName,
    'new member sees their credential!'
  )

  await Promise.all([
    p(member.close)(true),
    p(kaitiaki.close)(true)
  ])

  t.end()
})
