const { test, p, Testbot, Run } = require('./helpers')

test('atalaPrism.start', async t => {
  const run = Run(t)
  const { ssb } = await Testbot()

  await ssb.atalaPrism.start()
    .then(agent => {
      t.equal(agent.state, 'running', 'agent starts')
    })
    .catch(err => t.error(err, 'agent starts'))

  await run(
    'agent closes with ssb',
    p(ssb.close)(true)
  )

  t.end()
})

test('atalaPrism.start (twice)', async t => {
  const run = Run(t)

  const rand = Math.round(9999 * Math.random())
  const aliceName = 'alice-' + rand
  const bobName = 'bob-' + rand

  const alice = await Testbot({ name: aliceName })
  const bob = await Testbot({ name: bobName })

  await Promise.all([
    run(
      'start agent (alice)',
      alice.ssb.atalaPrism.start()
    ),
    run(
      'start agent (bob)',
      bob.ssb.atalaPrism.start()
    )
  ])

  await run(
    'agent closes with ssb',
    Promise.all([
      p(alice.ssb.close)(true),
      p(bob.ssb.close)(true)
    ])
  )

  t.end()
})

test('atalaPrism.start (bad config)', async t => {
  const { ssb } = await Testbot({
    atalaPrism: {
      mediatorDID: 'didINeedADIDHere?'
    }
  })

  await ssb.atalaPrism.start()
    .then(() => t.fail('should not start'))
    .catch(err => {
      t.match(
        err.message.replace(/\n/g, ''),
        /Invalid.*mediatorDID/, 'invalid config errors helpfully'
      )
      // console.log(err)
    })

  await p(ssb.close)(true)
  t.end()
})
