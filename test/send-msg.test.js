const { test, p } = require('./helpers')
const SDK = require('@hyperledger/identus-edge-agent-sdk')

// skipped because we don't use this at the moment

test.skip('atalaPrism send message', async t => {
  const apollo = new SDK.Apollo()
  const api = new SDK.ApiImpl()
  const castor = new SDK.Castor(apollo)

  const plutoConfig = {
    type: 'sqljs'
  }

  const pluto = new SDK.Pluto(plutoConfig)

  const mediatorDID = SDK.Domain.DID.fromString(
    'did:peer:2.Ez6LSghwSE437wnDE1pt3X6hVDUQzSjsHzinpX3XFvMjRAm7y.Vz6Mkhh1e5CEYYq6JBUcTZ6Cp2ranCWRrv7Yax3Le4N59R6dd.SeyJ0IjoiZG0iLCJzIjp7InVyaSI6Imh0dHA6Ly9sb2NhbGhvc3Q6ODA4MCIsImEiOlsiZGlkY29tbS92MiJdfX0.SeyJ0IjoiZG0iLCJzIjp7InVyaSI6IndzOi8vbG9jYWxob3N0OjgwODAvd3MiLCJhIjpbImRpZGNvbW0vdjIiXX19'
  )

  const didcomm = new SDK.DIDCommWrapper(apollo, castor, pluto)
  const mercury = new SDK.Mercury(castor, didcomm, api)
  const store = new SDK.PublicMediatorStore(pluto)
  const handler = new SDK.BasicMediatorHandler(mediatorDID, mercury, store)
  const manager = new SDK.ConnectionsManager(castor, mercury, pluto, handler)
  const seed = apollo.createRandomSeed()
  const agent = new SDK.Agent(
    apollo,
    castor,
    pluto,
    mercury,
    handler,
    manager,
    seed.seed
  )

  agent.addListener('message', (message) => {
    t.pass('Got new message')
    console.log('new message:', message)
  })

  await agent.start()
  t.equal(agent.state, 'running', 'agent is running')

  const secondaryDID = await agent.createNewPeerDID([], true)

  const msg = new SDK.BasicMessage(
    { content: 'Test Message' },
    secondaryDID,
    secondaryDID
  )

  await agent.sendMessage(msg.makeMessage())
    .catch(err => console.log('bad error', err))

  await p(setTimeout)(10000)

  await agent.stop()
  t.end()
})
