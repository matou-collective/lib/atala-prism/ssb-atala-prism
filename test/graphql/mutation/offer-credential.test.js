const CRUT = require('ssb-crut')

const { test, p, Testbot, Run, gql } = require('../../helpers')
const spec = require('../../../spec/auto-issuance')

test('graphQL - mutation offerCredential (with connection)', async t => {
  const run = Run(t)

  // <SETUP> kaitiaki (issuer) =============================================== //
  const rand = Math.round(9999 * Math.random())
  const kaitiakiName = 'kaitiaiki-' + rand
  let kaitiaki = await Testbot({ name: kaitiakiName })

  const tribeName = 'Whangaroa Papa Hapu'
  const { groupId: tribeId, poBoxId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // NOTE we have to restart the kaitiaki to install the issuer config
  const keys = kaitiaki.ssb.keys
  await p(kaitiaki.ssb.close)(true)
  const kaitiakiAgain = await Testbot({
    name: kaitiakiName,
    keys,
    rimraf: false,
    atalaPrism: {
      issuers: {
        [tribeId]: {
          tribeName,
          ISSUER_URL: process.env.ISSUER_URL,
          ISSUER_APIKEY: process.env.ISSUER_APIKEY
        }
      }
    }
  })
  kaitiaki = kaitiakiAgain
  // <SETUP> kaitiaki (issuer) =============================================== //

  await run('kaitiaki agent starts', kaitiaki.ssb.atalaPrism.start())

  const crut = new CRUT(kaitiaki.ssb, spec)

  await run(
    'offerCredential',
    kaitiaki.apollo.mutate({
      mutation: gql`mutation ($tribeId: String!, $poBoxId: String!, $feedId: String!, $claims: MembershipClaimInput!) {
        offerCredential (tribeId: $tribeId, poBoxId: $poBoxId, feedId: $feedId, claims: $claims)
      }`,
      variables: {
        tribeId,
        poBoxId,
        feedId: '@Etqowis6CI76dpqORYXSbZxBCOVHfsbVaPUShRleZc8=.ed25519',
        claims: {
          person: {
            fullName: 'Mix Irving',
            dateOfBirth: '1984/XX/XX'
          }
        }
      }
    })
  )

  const offers = await crut.list()

  t.deepEqual(
    offers.map(o => o.claims),
    [{
      person: {
        fullName: 'Mix Irving',
        dateOfBirth: '1984/XX/XX'
      },
      memberOf: {
        tribeId,
        tribeName
      }
    }]
  )

  await p(kaitiaki.ssb.close)(true)
  t.end()
})

test('graphQL - mutation offerCredential (connectionless)', async t => {
  const run = Run(t)

  // <SETUP> kaitiaki (issuer) =============================================== //
  const rand = Math.round(9999 * Math.random())
  const kaitiakiName = 'kaitiaiki-' + rand
  let kaitiaki = await Testbot({ name: kaitiakiName })

  const tribeName = 'Whangaroa Papa Hapu'
  const { groupId: tribeId, poBoxId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // NOTE we have to restart the kaitiaki to install the issuer config
  const keys = kaitiaki.ssb.keys
  await p(kaitiaki.ssb.close)(true)
  const kaitiakiAgain = await Testbot({
    name: kaitiakiName,
    keys,
    rimraf: false,
    atalaPrism: {
      issuers: {
        [tribeId]: {
          tribeName,
          ISSUER_URL: process.env.ISSUER_URL,
          ISSUER_APIKEY: process.env.ISSUER_APIKEY
        }
      }
    }
  })
  kaitiaki = kaitiakiAgain
  // <SETUP> kaitiaki (issuer) =============================================== //

  await run('kaitiaki agent starts', kaitiaki.ssb.atalaPrism.start())

  const crut = new CRUT(kaitiaki.ssb, spec)

  await run(
    'offerCredential',
    kaitiaki.apollo.mutate({
      mutation: gql`mutation ($tribeId: String!, $poBoxId: String!, $feedId: String!, $claims: MembershipClaimInput!, $connectionless: Boolean) {
        offerCredential (tribeId: $tribeId, poBoxId: $poBoxId, feedId: $feedId, claims: $claims, connectionless: $connectionless)
      }`,
      variables: {
        tribeId,
        poBoxId,
        feedId: '@Etqowis6CI76dpqORYXSbZxBCOVHfsbVaPUShRleZc8=.ed25519',
        claims: {
          person: {
            fullName: 'Mix Irving',
            dateOfBirth: '1984/XX/XX'
          }
        },
        connectionless: true
      }
    })
  )

  const offers = await crut.list()

  t.deepEqual(
    offers.map(o => o.claims),
    [{
      person: {
        fullName: 'Mix Irving',
        dateOfBirth: '1984/XX/XX'
      },
      memberOf: {
        tribeId,
        tribeName
      }
    }]
  )

  await p(kaitiaki.ssb.close)(true)
  t.end()
})
