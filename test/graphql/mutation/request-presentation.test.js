const CRUT = require('ssb-crut')

const { test, p, Testbot, Run, gql } = require('../../helpers')
const autoPresentationSpec = require('../../../spec/auto-presentation')

test('graphQL - mutation requestPresentation (with connection)', async t => {
  const run = Run(t)

  // <SETUP> kaitiaki (issuer) =============================================== //
  const rand = Math.round(9999 * Math.random())
  const kaitiakiName = 'kaitiaiki-' + rand
  let kaitiaki = await Testbot({ name: kaitiakiName })

  const tribeName = 'Whangaroa Papa Hapu'
  const { groupId: tribeId, poBoxId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // NOTE we have to restart the kaitiaki to install the issuer config
  const keys = kaitiaki.ssb.keys
  await p(kaitiaki.ssb.close)(true)
  const kaitiakiAgain = await Testbot({
    name: kaitiakiName,
    keys,
    rimraf: false,
    atalaPrism: {
      verifiers: {
        [tribeId]: {
          tribeName,
          VERIFIER_URL: process.env.VERIFIER_URL,
          VERIFIER_APIKEY: process.env.VERIFIER_APIKEY
        }
      }
    }
  })
  kaitiaki = kaitiakiAgain
  // <SETUP> kaitiaki (issuer) =============================================== //

  await run('kaitiaki agent starts', kaitiaki.ssb.atalaPrism.start())

  const crut = new CRUT(kaitiaki.ssb, autoPresentationSpec)
  const feedId = '@Etqowis6CI76dpqORYXSbZxBCOVHfsbVaPUShRleZc8=.ed25519'

  await run(
    'requestPresentation',
    kaitiaki.apollo.mutate({
      mutation: gql`mutation ($tribeId: String!, $poBoxId: String!, $feedId: String!) {
        requestPresentation (tribeId: $tribeId, poBoxId: $poBoxId, feedId: $feedId)
      }`,
      variables: {
        tribeId,
        poBoxId,
        feedId
      }
    })
  )

  const offers = await crut.list()

  t.deepEqual(
    offers.map(o => o.recps),
    [[poBoxId, feedId]],
    'api worked'
  )

  await p(setTimeout)(1000)

  await p(kaitiaki.ssb.close)(true)
  t.end()
})

test('graphQL - mutation requestPresentation (connectionless)', async t => {
  const run = Run(t)

  // <SETUP> kaitiaki (issuer) =============================================== //
  const rand = Math.round(9999 * Math.random())
  const kaitiakiName = 'kaitiaiki-' + rand
  let kaitiaki = await Testbot({ name: kaitiakiName })

  const tribeName = 'Whangaroa Papa Hapu'
  const { groupId: tribeId, poBoxId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })

  // NOTE we have to restart the kaitiaki to install the issuer config
  const keys = kaitiaki.ssb.keys
  await p(kaitiaki.ssb.close)(true)
  const kaitiakiAgain = await Testbot({
    name: kaitiakiName,
    keys,
    rimraf: false,
    atalaPrism: {
      verifiers: {
        [tribeId]: {
          tribeName,
          VERIFIER_URL: process.env.VERIFIER_URL,
          VERIFIER_APIKEY: process.env.VERIFIER_APIKEY
        }
      }
    }
  })
  kaitiaki = kaitiakiAgain
  // <SETUP> kaitiaki (issuer) =============================================== //

  await run('kaitiaki agent starts', kaitiaki.ssb.atalaPrism.start())

  const crut = new CRUT(kaitiaki.ssb, autoPresentationSpec)
  const feedId = '@Etqowis6CI76dpqORYXSbZxBCOVHfsbVaPUShRleZc8=.ed25519'

  await run(
    'requestPresentation',
    kaitiaki.apollo.mutate({
      mutation: gql`mutation ($tribeId: String!, $poBoxId: String!, $feedId: String!, $connectionless: Boolean) {
        requestPresentation (tribeId: $tribeId, poBoxId: $poBoxId, feedId: $feedId, connectionless: $connectionless)
      }`,
      variables: {
        tribeId,
        poBoxId,
        feedId,
        connectionless: true
      }
    })
  )

  const offers = await crut.list()

  t.deepEqual(
    offers.map(o => o.recps),
    [[poBoxId, feedId]],
    'api worked'
  )

  await p(setTimeout)(1000)

  await p(kaitiaki.ssb.close)(true)
  t.end()
})
