const CRUT = require('ssb-crut')

const { test, p, replicate, Testbot, Run, gql } = require('../../helpers')
const autoPresentationSpec = require('../../../spec/auto-presentation')

const { PRES_COMPLETE } = autoPresentationSpec.states

test('graphql query - registration.credentialPresentations', async t => {
  const run = Run(t)

  const rand = Math.round(9999 * Math.random())
  const newMemberName = 'new-member-' + rand
  const kaitiakiName = 'kaitiaiki-' + rand

  const { ssb: member } = await Testbot({ name: newMemberName })

  // <SETUP> otherKaitiaki (verifier) ======================================== //
  let { ssb: otherKaitiaki } = await Testbot({ name: kaitiakiName })
  const otherTribeName = 'Whangaroa whānau'
  const {
    groupId: otherTribeId,
    poBoxId: otherPOBoxId
  } = await p(otherKaitiaki.tribes.create)({ addPOBox: true })

  const otherKeys = otherKaitiaki.keys
  await p(otherKaitiaki.close)(true)
  const otherKaitiakiAgain = await Testbot({
    name: kaitiakiName,
    keys: otherKeys,
    rimraf: false,
    atalaPrism: {
      verifiers: {
        [otherTribeId]: {
          tribeName: otherTribeName,
          VERIFIER_URL: process.env.VERIFIER_URL,
          VERIFIER_APIKEY: process.env.VERIFIER_APIKEY
        }
      }
    }
  })
  otherKaitiaki = otherKaitiakiAgain.ssb
  const apollo = otherKaitiakiAgain.apollo
  // </SETUP> otherKaitiaki (verifier) ======================================= //

  await run('otherKaitiaki agent starts', otherKaitiaki.atalaPrism.start())

  replicate({ from: otherKaitiaki, to: member, live: true, log: false })
  replicate({ from: member, to: otherKaitiaki, live: true, log: false })

  await run(
    'member sends registration to join otherTribe',
    p(member.registration.tribe.create)(
      otherTribeId, { recps: [otherPOBoxId, member.id] })
  )

  const autoPres = new CRUT(otherKaitiaki, autoPresentationSpec)

  await run(
    'make a presentation record (fake)',
    autoPres.create({
      JWT: 'eyJhbGciOiJFUzI1NksiLCJ0eXAiOiJKV1QifQ.eyJpYXQiOjE3MDc3ODcyNDUsImlzcyI6ImRpZDpwcmlzbTpiZjI2ZTkyMTUzMDc3OTQ0MTRhMGJiNjhlYjA4YjQ3MWIwMmZkMWJiN2FkZWNiMTQxMmM0NDdkMjQzMzg3YThkOkN0OEJDdHdCRW5RS0gyRjFkR2hsYm5ScFkyRjBhVzl1WVhWMGFHVnVkR2xqWVhScGIyNUxaWGtRQkVKUENnbHpaV053TWpVMmF6RVNJSm8zSjh2dUVaRVZKdkdIYVZrZTNsczgtdDhNSnBPV1NETjhmRWZXQ2NaMEdpQWMzbURjN1lpYXEwcV95VXc5cEpOTGYzeV9Pd3RWMUY3QnFWeHN1UUpwUVJKa0NnOXRZWE4wWlhKdFlYTjBaWEpMWlhrUUFVSlBDZ2x6WldOd01qVTJhekVTSUpvM0o4dnVFWkVWSnZHSGFWa2UzbHM4LXQ4TUpwT1dTRE44ZkVmV0NjWjBHaUFjM21EYzdZaWFxMHFfeVV3OXBKTkxmM3lfT3d0VjFGN0JxVnhzdVFKcFFRIiwiYXVkIjoid3d3LmFoYXUuaW8iLCJub25jZSI6ImUxMjQ0N2IyLTBjZWItNDczOS04NWQxLTI4NWVhZDA4YTU1YyIsInZwIjp7IkBjb250ZXh0IjpbImh0dHBzOi8vd3d3LnczLm9yZy8yMDE4L3ByZXNlbnRhdGlvbnMvdjEiXSwidHlwZSI6WyJWZXJpZmlhYmxlUHJlc2VudGF0aW9uIl0sInZlcmlmaWFibGVDcmVkZW50aWFsIjpbImV5SmhiR2NpT2lKRlV6STFOa3NpZlEuZXlKcGMzTWlPaUprYVdRNmNISnBjMjA2T1RaaU56aGtOV1kzWVRFM05HTTRaR0UwWVdWbE5HWTVObVk1Tmpsak1HSmxPR1V4TW1KbU16VTNOall3TlRsbVlqUTROVGc1WkdNeU56Z3dNekptWWpwRGNsRkNRM0pGUWtWcVowdENSM1JzWlZSRlVVSkZiM1ZEWjJ4NldsZE9kMDFxVlRKaGVrVlRTVkZKU0UxU1pXUnJaR1J4Y1V0SVFXZGFRbHB4YTJoVVdsQmlTSEl6VW5oM2VtRXRabkEyTWw5VWFDMWhlRWswUTJkU2NscFlhM2xGUVVwTFRHZHZTbU15Vm1walJFa3hUbTF6ZUVWcFJVUXhOVmxGTTNSSlN6bEhVR0kyVFY5SlZqUmFkaTFuZGpWUGRrOWZka0ZFWmw5UlRYRXdTRUV5UjBNd1UwOTNiMGhpVjBaNlpFZFdlVTFDUVVKVGFUUkxRMWhPYkZrelFYbE9WRnB5VFZKSmFFRTFURFZTWmxvNWJXY3RjRU15Y1ZWVFp6QklUVUZmZGtjNVNUazRTRlJvZG5relVXczRXVXhyZDNBd0lpd2ljM1ZpSWpvaVpHbGtPbkJ5YVhOdE9tSm1NalpsT1RJeE5UTXdOemM1TkRReE5HRXdZbUkyT0dWaU1EaGlORGN4WWpBeVptUXhZbUkzWVdSbFkySXhOREV5WXpRME4yUXlORE16T0RkaE9HUTZRM1E0UWtOMGQwSkZibEZMU0RKR01XUkhhR3hpYmxKd1dUSkdNR0ZYT1hWWldGWXdZVWRXZFdSSGJHcFpXRkp3WWpJMVRGcFlhMUZDUlVwUVEyZHNlbHBYVG5kTmFsVXlZWHBGVTBsS2J6TktPSFoxUlZwRlZrcDJSMGhoVm10bE0yeHpPQzEwT0UxS2NFOVhVMFJPT0daRlpsZERZMW93UjJsQll6TnRSR00zV1dsaGNUQnhYM2xWZHpsd1NrNU1aak41WDA5M2RGWXhSamRDY1ZaNGMzVlJTbkJSVWtwclEyYzVkRmxZVGpCYVdFcDBXVmhPTUZwWVNreGFXR3RSUVZWS1VFTm5iSHBhVjA1M1RXcFZNbUY2UlZOSlNtOHpTamgyZFVWYVJWWktka2RJWVZaclpUTnNjemd0ZERoTlNuQlBWMU5FVGpobVJXWlhRMk5hTUVkcFFXTXpiVVJqTjFscFlYRXdjVjk1VlhjNWNFcE9UR1l6ZVY5UGQzUldNVVkzUW5GV2VITjFVVXB3VVZFaUxDSnVZbVlpT2pFM01EYzNPRGN5TWprc0luWmpJanA3SW1OeVpXUmxiblJwWVd4VGRXSnFaV04wSWpwN0luQmxjbk52YmlJNmV5Sm1kV3hzVG1GdFpTSTZJa0psYmlCVVlXbHlaV0VpTENKa1lYUmxUMlpDYVhKMGFDSTZJakU1T0RkY0wxaFlYQzlZV0NKOUxDSnRaVzFpWlhKUFppSTZleUowY21saVpVbGtJam9pSldsUFR6bG9VSGhLWVhnNFQxSm1iWFUwUVhsQmRIRllVbGs1ZFROM04wMHhkREUyU1ZGWWFXNTRWelE5TG1Oc2IyRnJaV1FpTENKMGNtbGlaVTVoYldVaU9pSlhhR0Z1WjJGeWIyRWdVR0Z3WVNCSVlYQjFJbjBzSW1sa0lqb2laR2xrT25CeWFYTnRPbUptTWpabE9USXhOVE13TnpjNU5EUXhOR0V3WW1JMk9HVmlNRGhpTkRjeFlqQXlabVF4WW1JM1lXUmxZMkl4TkRFeVl6UTBOMlF5TkRNek9EZGhPR1E2UTNRNFFrTjBkMEpGYmxGTFNESkdNV1JIYUd4aWJsSndXVEpHTUdGWE9YVlpXRll3WVVkV2RXUkhiR3BaV0ZKd1lqSTFURnBZYTFGQ1JVcFFRMmRzZWxwWFRuZE5hbFV5WVhwRlUwbEtiek5LT0haMVJWcEZWa3AyUjBoaFZtdGxNMnh6T0MxME9FMUtjRTlYVTBST09HWkZabGREWTFvd1IybEJZek50UkdNM1dXbGhjVEJ4WDNsVmR6bHdTazVNWmpONVgwOTNkRll4UmpkQ2NWWjRjM1ZSU25CUlVrcHJRMmM1ZEZsWVRqQmFXRXAwV1ZoT01GcFlTa3hhV0d0UlFWVktVRU5uYkhwYVYwNTNUV3BWTW1GNlJWTkpTbTh6U2poMmRVVmFSVlpLZGtkSVlWWnJaVE5zY3pndGREaE5TbkJQVjFORVRqaG1SV1pYUTJOYU1FZHBRV016YlVSak4xbHBZWEV3Y1Y5NVZYYzVjRXBPVEdZemVWOVBkM1JXTVVZM1FuRldlSE4xVVVwd1VWRWlmU3dpZEhsd1pTSTZXeUpXWlhKcFptbGhZbXhsUTNKbFpHVnVkR2xoYkNKZExDSkFZMjl1ZEdWNGRDSTZXeUpvZEhSd2N6cGNMMXd2ZDNkM0xuY3pMbTl5WjF3dk1qQXhPRnd2WTNKbFpHVnVkR2xoYkhOY0wzWXhJbDE5ZlEuVWlBckNBMUF2U0hsUnNxVDFHUlZ6T09aRG1FbkpxTEg5cnY4VjJhaF8tNnFyQWUwcFllRF9RTXBiS1lwbTBjYWYyUnRiblpZLWNYNlFHTHA4b0lBTkEiXX19.e0JCNNee-QZnDB5IxWi3M4T4dUhmkGJxrzPy857jLeP_5akpdAicqOci7-7Hh6az2MB0MBE9Xt2ifFGP-xxY6Q',
      state: PRES_COMPLETE,
      recps: [otherPOBoxId, member.id]
    })
  )

  const res = await run(
    'fetch registrations',
    apollo.query({
      query: gql` query($groupId: String) {
        listGroupApplications(groupId: $groupId) {
          credentialPresentations {
            state
            iat
            iss
            aud
            nonce
            credentials {
              sub
              iss
              issuanceDate
              # issKnown
              credentialSubject {
                person {
                  fullName
                  dateOfBirth
                }
                memberOf {
                  tribeId
                  tribeName
                }
              }
            }
          }
        }
      }`,
      variables: {
        groupId: otherTribeId
      }
    })
  )
  // console.log(JSON.stringify(res, null, 2))

  const presentation = res.data.listGroupApplications[0].credentialPresentations[0]

  t.equal(presentation.state, 'presentation complete', 'shows state')
  t.deepEqual(
    presentation.credentials[0].credentialSubject.person,
    {
      fullName: 'Ben Tairea',
      dateOfBirth: '1987/XX/XX'
    },
    'seems legit'
  )

  t.doesNotThrow(() => {
    new Date(presentation.credentials[0].issuanceDate).toDateString()
  }, 'Issuance date should parse to a date string')

  await p(setTimeout)(1000)

  await Promise.all([
    p(member.close)(true),
    p(otherKaitiaki.close)(true)
  ])

  t.end()
})
