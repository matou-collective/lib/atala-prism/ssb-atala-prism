const pull = require('pull-stream')

const { test, p, replicate, Testbot, Run, makeBreakpoint, gql } = require('../../helpers')
const spec = require('../../../spec/auto-issuance')
const { CRED_COMPLETE } = spec.states

async function setup (t, claims) {
  const run = Run(t)

  const rand = Math.round(9999 * Math.random())
  const newMemberName = 'new-member-' + rand
  const kaitiakiName = 'kaitiaiki-' + rand

  const member = await Testbot({ name: newMemberName })
  let kaitiaki = await Testbot({ name: kaitiakiName })

  const { groupId: tribeId, poBoxId } = await p(kaitiaki.ssb.tribes.create)({ addPOBox: true })
  const tribeName = 'Whangaroa Papa Hapu'

  // NOTE we have to restart the kaitiaki to install the issuer config
  const keys = kaitiaki.ssb.keys
  await p(kaitiaki.ssb.close)()
  const kaitiakiAgain = await Testbot({
    name: kaitiakiName,
    keys,
    rimraf: false,
    atalaPrism: {
      issuers: {
        [tribeId]: {
          tribeName,
          ISSUER_URL: process.env.ISSUER_URL,
          ISSUER_APIKEY: process.env.ISSUER_APIKEY
        }
      }
    }
  })
  kaitiaki = kaitiakiAgain

  await Promise.all([
    run('member agent starts', member.ssb.atalaPrism.start()),
    run('kaitiaki agent starts', kaitiaki.ssb.atalaPrism.start())
  ])

  replicate({ from: kaitiaki.ssb, to: member.ssb, live: true, log: false })
  replicate({ from: member.ssb, to: kaitiaki.ssb, live: true, log: false })

  // this breakpoint is something we can "await"
  // it will only by "DONE" when the breakpoint.resolve is called
  const breakpoint = makeBreakpoint()
  pull(
    kaitiaki.ssb.messagesByType({
      type: spec.type,
      private: true,
      live: true
    }),
    pull.filter(m => !m.sync),
    pull.filter(m => {
      const state = m.value.content?.state?.set
      return state === CRED_COMPLETE
    }),
    pull.drain(breakpoint.resolve)
    // this says "we will be DONE with this breakpoint, when we
    // see our first CRED_COMPLETE state
  )

  await kaitiaki.ssb.atalaPrism.offerCredential(
    tribeId,
    poBoxId,
    member.ssb.id,
    claims
  )

  console.log('waiting at breakpoint...')
  await breakpoint

  return {
    kaitiaki,
    member,
    tribeId,
    tribeName
  }
}

test('graphQL - query verifiableCredentials', { objectPrintDepth: 10 }, async t => {
  const run = Run(t)

  const claims = {
    person: {
      fullName: 'Ben Tairea',
      dateOfBirth: '1987/XX/XX'
    }
  }

  const { kaitiaki, member, tribeId, tribeName } = await setup(t, claims)

  const res = await run(
    'run query',
    member.apollo.query({
      query: gql` query {
        verifiableCredentials {
          id
          sub
          iss
          credentialSubject
          credentialStatus { id }
          issuanceDate
        }
      }`
    })
  )

  t.deepEqual(
    res.data.verifiableCredentials.map(vc => vc.credentialSubject),
    [{
      id: res.data.verifiableCredentials[0].credentialSubject.id,
      memberOf: {
        tribeId,
        tribeName
      },
      ...claims
    }],
    'credentials readable!'
  )

  t.doesNotThrow(() => {
    new Date(res.data.verifiableCredentials[0].issuanceDate).toDateString()
  }, 'Issuance date should parse to a date string')

  await Promise.all([
    p(kaitiaki.ssb.close)(true),
    p(member.ssb.close)(true)
  ])
  t.end()
})
