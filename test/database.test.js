const { test, p, Testbot, Run } = require('./helpers')

test('database', async t => {
  const run = Run(t)

  let { ssb } = await Testbot({
    name: 'atala-prism',
    rimraf: true
  })
  await run('start agent', ssb.atalaPrism.start())

  const peerDIDs = await run(
    'get all peer dids',
    ssb.atalaPrism.getAllPeerDIDs()
  )

  // NOTE: recent changes it is now returning 2 instead of 1
  t.equal(peerDIDs.length, 2, 'getAllPeerDIDs returns one DID')

  let prismDID

  prismDID = await run('get all prism dids', ssb.atalaPrism.getAllPrismDIDs())

  if (!prismDID.length) {
    await run(
      'created new PrismDID',
      ssb.atalaPrism.createNewPrismDID('test', [])
    )
    prismDID = await ssb.atalaPrism.getAllPrismDIDs()
  }

  t.true(prismDID.length >= 1, 'calling getAllPrismDIDs returns at least 1 prismDID')

  await p(ssb.close)(true)

  ssb = await Testbot({
    name: 'atala-prism',
    rimraf: false
  }).then(res => res.ssb)

  await run('start agent (again)', ssb.atalaPrism.start())

  const peerDIDsAgain = await run(
    'get all peer dids (again)',
    ssb.atalaPrism.getAllPeerDIDs()
  )

  t.deepEqual(peerDIDs, peerDIDsAgain, 'database persists peerDIDs')

  await p(ssb.close)(true)

  t.end()
})
