const { test, p, replicate, Testbot, Run, makeBreakpoint } = require('../helpers')

const pull = require('pull-stream')
const CRUT = require('ssb-crut')

const autoIssuanceSpec = require('../../spec/auto-issuance')
const autoPresentationSpec = require('../../spec/auto-presentation')

const { CRED_COMPLETE } = autoIssuanceSpec.states
const { PRES_COMPLETE } = autoPresentationSpec.states

test('auto-presentation (from new member perspective)', async t => {
  const run = Run(t)

  const rand = Math.round(9999 * Math.random())
  const newMemberName = 'new-member-' + rand
  const kaitiakiName = 'kaitiaiki-' + rand

  const { ssb: member } = await Testbot({ name: newMemberName })

  // <SETUP> kaitiaki (issuer) =============================================== //
  let { ssb: kaitiaki } = await Testbot({ name: kaitiakiName })

  const tribeName = 'Whangaroa Papa Hapu'
  const { groupId: tribeId, poBoxId } = await p(kaitiaki.tribes.create)({ addPOBox: true })

  // NOTE we have to restart the kaitiaki to install the issuer config
  const keys = kaitiaki.keys
  await p(kaitiaki.close)(true)
  const { ssb: kaitiakiAgain } = await Testbot({
    name: kaitiakiName,
    keys,
    rimraf: false,
    atalaPrism: {
      issuers: {
        [tribeId]: {
          tribeName,
          ISSUER_URL: process.env.ISSUER_URL,
          ISSUER_APIKEY: process.env.ISSUER_APIKEY
        }
      }
    }
  })
  kaitiaki = kaitiakiAgain
  // <SETUP> kaitiaki (issuer) =============================================== //

  await Promise.all([
    run('member agent starts', member.atalaPrism.start()),
    run('kaitiaki agent starts', kaitiaki.atalaPrism.start())
  ])

  replicate({ from: kaitiaki, to: member, live: true, log: false })
  replicate({ from: member, to: kaitiaki, live: true, log: false })

  // this breakpoint is something we can "await"
  // it will only by "DONE" when the breakpoint.resolve is called
  const breakpointIssuing = makeBreakpoint()
  pull(
    kaitiaki.messagesByType({
      type: autoIssuanceSpec.type,
      private: true,
      live: true
    }),
    pull.filter(m => !m.sync),
    pull.filter(m => {
      const state = m.value.content?.state?.set
      return state === CRED_COMPLETE
    }),
    pull.drain(breakpointIssuing.resolve)
    // this says "we will be DONE with this breakpoint, when we
    // see our first CRED_COMPLETE state
  )

  const claims = {
    person: {
      fullName: 'Ben Tairea',
      dateOfBirth: '1987/XX/XX'
    }
  }
  await kaitiaki.atalaPrism.offerCredential(
    tribeId,
    poBoxId,
    member.id,
    claims
  )

  console.log('waiting for auto-issue of VC...')
  await breakpointIssuing

  // <SETUP> otherKaitiaki (verifier) ======================================== //
  const otherKaitiaikiName = 'other-kaitiaki-' + rand
  let { ssb: otherKaitiaki } = await Testbot({ name: otherKaitiaikiName })
  const otherTribeName = 'Whangaroa whānau'
  const {
    groupId: otherTribeId,
    poBoxId: otherPOBoxId
  } = await p(otherKaitiaki.tribes.create)({ addPOBox: true })

  const otherKeys = otherKaitiaki.keys
  await p(otherKaitiaki.close)(true)
  const { ssb: otherKaitiakiAgain } = await Testbot({
    name: otherKaitiaikiName,
    keys: otherKeys,
    rimraf: false,
    atalaPrism: {
      verifiers: {
        [otherTribeId]: {
          tribeName: otherTribeName,
          VERIFIER_URL: process.env.VERIFIER_URL,
          VERIFIER_APIKEY: process.env.VERIFIER_APIKEY
        }
      }
    }
  })
  otherKaitiaki = otherKaitiakiAgain
  // </SETUP> otherKaitiaki (verifier) ======================================= //

  await run('otherKaitiaki agent starts', otherKaitiaki.atalaPrism.start())

  replicate({ from: otherKaitiaki, to: member, live: true, log: false })
  replicate({ from: member, to: otherKaitiaki, live: true, log: false })

  const breakpointPresenting = makeBreakpoint()
  pull(
    member.messagesByType({
      type: autoPresentationSpec.type,
      private: true,
      live: true
    }),
    pull.filter(m => !m.sync),
    pull.filter(m => {
      const state = m.value.content?.state?.set
      return state === PRES_COMPLETE
    }),
    pull.drain(breakpointPresenting.resolve)
  )

  const recordId = await run(
    'otherKaitiaki request presentation',
    otherKaitiaki.atalaPrism.requestPresentation(
      otherTribeId,
      otherPOBoxId,
      member.id
    )
  )

  console.log('waiting for auto-presentation of VC...')
  await breakpointPresenting

  let record = await new CRUT(otherKaitiaki, autoPresentationSpec).read(recordId)
  t.equal(record.state, PRES_COMPLETE, 'otherKaitiaki sees presentation complete')

  record = await new CRUT(member, autoPresentationSpec).read(recordId)
  t.equal(record.state, PRES_COMPLETE, 'member sees presentation complete')

  // TODO test presentation can be loaded?
  await p(setTimeout)(1000)

  await Promise.all([
    p(kaitiaki.close)(true),
    p(member.close)(true),
    p(otherKaitiaki.close)(true)
  ])

  t.end()
})
