const test = require('tape')
const { promisify: p } = require('util')
const gql = require('graphql-tag')
const { replicate } = require('scuttle-testbot')

const Testbot = require('./testbot')
const issuer = require('./issuer')
const verifier = require('./verifier')

module.exports = {
  gql,
  p,
  test,
  replicate,
  Testbot,

  // helpers for entities the SDK doesn't run
  issuer,
  verifier,

  // helper for running promise tests
  Run (t) {
    return function run (msg, promise) {
      if (promise.constructor !== Promise) {
        t.pass(msg)
        return Promise.resolve(promise)
      }

      return promise
        .then(result => {
          t.pass(msg)
          return result
        })
        .catch(err => {
          t.error(err, msg)
        })
    }
  },

  RetryUntil (t) {
    return async function retryUntil (msg, fn, opts = {}) {
      const {
        maxTries = 10,
        delay = 1000
      } = opts

      let triesRemaining = maxTries

      while (triesRemaining) {
        const result = await fn()
        if (result === true) {
          t.pass(msg)
          return
        }

        triesRemaining--
        await p(setTimeout)(delay)
        console.log(`? ${msg} (retry ${maxTries - triesRemaining})`)
      }

      t.fail(msg)
    }
  },

  makeBreakpoint () {
    let _resolve

    const breakpoint = new Promise((resolve) => {
      _resolve = resolve
    })
    breakpoint.resolve = _resolve

    return breakpoint
  }

  // ## Usage
  //
  // ```js
  // const breakpoint = makeBreakpoint()
  //
  // listenForEvent(event => {
  //   breakpoint.resolve()
  // })
  //
  // await breakpoint
  //
  // t.equal(......)
  // t.end()
  // ```
}
