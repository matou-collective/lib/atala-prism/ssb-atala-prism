// NOTE: ENV are being set in testbot

module.exports = {
  // createPrismDID,
  createConnectionInvitation,
  getConnectionRecord,
  issueCredential,
  getPrismDID
}

function createConnectionInvitation () {
  return fetch(`${process.env.ISSUER_URL}/connections`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      apiKey: process.env.ISSUER_APIKEY
    },
    body: JSON.stringify({
      label: 'Connect Issuer and Member'
    })
  })
    .then((response) => response.json())
    .then((json) => {
      return json.invitation
    })
}

function getConnectionRecord (connectionId) {
  return fetch(`${process.env.ISSUER_URL}/connections/${connectionId}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      apiKey: process.env.ISSUER_APIKEY
    }
  })
    .then((response) => response.json())
    .then((json) => {
      return json
    })
}

function getPrismDID () {
  // get issuers PrismDID
  return fetch(`${process.env.ISSUER_URL}/did-registrar/dids`, {
    method: 'GET',
    headers: {
      accept: 'application/json',
      'Content-Type': 'application/json',
      apiKey: process.env.ISSUER_APIKEY
    }
  })
    .then(response => response.json())
    .then(async (json) => {
      // NOTE: update to use published DID's
      return json.contents[0]?.longFormDid ?? await createPrismDID()
    })
}

// create issuer PrismDID
function createPrismDID () {
  console.log('no dids found. Creating one...')
  return fetch(`${process.env.ISSUER_URL}/did-registrar/dids`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'Content-Type': 'application/json',
      apiKey: process.env.ISSUER_APIKEY
    },
    body: JSON.stringify({
      documentTemplate: {
        publicKeys: [
          {
            id: 'auth-1',
            purpose: 'authentication'
          },
          {
            id: 'assert-1',
            purpose: 'assertionMethod'
          }
        ],
        services: []
      }
    })
  })
    .then(response => response.json())
    .then(json => {
      return json.longFormDid
    })
}

function issueCredential (didPrism, connectionId, claims) {
  return fetch(`${process.env.ISSUER_URL}/issue-credentials/credential-offers`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'Content-Type': 'application/json',
      apiKey: process.env.ISSUER_APIKEY
    },
    body: JSON.stringify({
      claims,
      issuingDID: didPrism,
      connectionId,
      automaticIssuance: true
    })
  })
    .then(response => response.json())
    .then(json => {
      return json
    })
}
