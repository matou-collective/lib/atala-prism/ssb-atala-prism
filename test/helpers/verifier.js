// NOTE: ENV are being set in testbot

module.exports = {
  // createPrismDID,
  createConnectionInvitation,
  getConnectionRecord,
  createPresentationRequest,
  getPresentation,
  acceptPresentation
}

function createConnectionInvitation () {
  return fetch(`${process.env.VERIFIER_URL}/connections`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      apiKey: process.env.VERIFIER_APIKEY
    },
    body: JSON.stringify({
      label: 'Connect Verifier and Member'
    })
  })
    .then((response) => response.json())
    .then((json) => json.invitation)
}

function getConnectionRecord (connectionId) {
  return fetch(`${process.env.VERIFIER_URL}/connections/${connectionId}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      apiKey: process.env.VERIFIER_APIKEY
    }
  })
    .then((response) => response.json())
}

function createPresentationRequest (connectionId, trustedIssuerDID) {
  return fetch(`${process.env.VERIFIER_URL}/present-proof/presentations`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'Content-Type': 'application/json',
      apiKey: process.env.VERIFIER_APIKEY
    },
    body: JSON.stringify({
      connectionId,
      options: {
        challenge: '1234567890',
        domain: process.env.VERIFIER_URL
      },
      proofs: [] // TODO: figure out how to manage VC schemas https://docs.atalaprism.io/agent-api/#tag/Present-Proof/operation/requestPresentation
    })
  })
    .then(response => response.json())
}

function getPresentation (presentationId) {
  return fetch(`${process.env.VERIFIER_URL}/present-proof/presentations/${presentationId}`, {
    method: 'GET',
    headers: {
      accept: 'application/json',
      'Content-Type': 'application/json',
      apiKey: process.env.VERIFIER_APIKEY
    }
  })
    .then(response => response.json())
}

function acceptPresentation (presentationId) {
  return fetch(`${process.env.VERIFIER_URL}/present-proof/presentations/${presentationId}`, {
    method: 'POST',
    headers: {
      accept: 'application/json',
      'Content-Type': 'application/json',
      apiKey: process.env.VERIFIER_APIKEY
    },
    body: JSON.stringify({
      action: 'request-accept'
      // proofId:
    })
  })
}
