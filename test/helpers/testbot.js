const path = require('path')
const dotenv = require('dotenv')

const scuttleTestbot = require('scuttle-testbot')
const ahauServer = require('ahau-graphql-server')
const AhauClient = require('ahau-graphql-client')

const envFile = process.env.NODE_ENV === 'production' ? '.env.production' : '.env'
const repoRootDir = path.resolve(__dirname, '..', '..')
const envFilePath = path.join(repoRootDir, envFile)

dotenv.config({ path: envFilePath })

const MEDIATOR_DID_URL = new URL('did', process.env.MEDIATOR_URL) // TODO default?

module.exports = async function Testbot (opts = {}) {
  const stack = scuttleTestbot
    .use(require('ssb-backlinks'))
    .use(require('ssb-query'))
    .use(require('ssb-profile'))
    .use(require('ssb-tribes'))
    .use(require('ssb-tribes-registration'))
    .use(require('ssb-blobs'))
    .use(require('../..')) // this plugin

  const mediatorDID = await fetch(MEDIATOR_DID_URL)
    .then(res => res.text())

  // scuttlebutt instance
  const ssb = stack({
    db1: true,
    ...opts,
    atalaPrism: {
      mediatorDID,
      ...(opts?.atalaPrism || {})
    }
  })

  const graphqlPort = 3000 + Math.random() * 7000 | 0

  const profile = require('@ssb-graphql/profile')(ssb)
  // graphql API
  const httpServer = await ahauServer({
    schemas: [
      require('@ssb-graphql/main')(ssb),
      profile,
      require('@ssb-graphql/tribes')(ssb, { ...profile.gettersWithCache }),
      require('../../graphql')(ssb)
    ],
    context: {},
    port: graphqlPort
  })
  ssb.close.hook(function (close, args) {
    httpServer.close()
    close.apply(this, args)
  })

  const apollo = new AhauClient(graphqlPort, { isTesting: true, fetch })

  return {
    ssb,
    apollo
  }
}
