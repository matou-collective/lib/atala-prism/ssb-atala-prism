#!/usr/bin/env bash

# See: https://github.com/hyperledger/identus/releases/tag/v2.13

export MEDIATOR_VERSION=0.15.0
export AGENT_VERSION=1.39.0
export PRISM_NODE_VERSION=2.5.0

# Detect OS
unameOut="$(uname -s)"
case "${unameOut}" in
  Linux*)
    export DOCKERHOST="$(ip addr show $(ip route show default | awk '/default/ {print $5}') | grep 'inet ' | awk '{print $2}' | cut -d/ -f1)"
    ;;
  Darwin*)
    export DOCKERHOST="$(ipconfig getifaddr $(route get default | grep interface | awk '{print $2}'))"
    ;;
  *)
    echo "unknown OS: $unameOut";
    exit 1;
    ;;
esac

export SERVICE_ENDPOINTS="http://$DOCKERHOST:8080;ws://$DOCKERHOST:8080/ws"
