#!/usr/bin/env bash

BASE=./test/docker
source $BASE/set-env.sh

# Start Mediator
cd test/docker/mediator
docker compose up --detach
cd ../../..
# NOTE: tried the below, but were seeing problems with initdb.js being referenced correctly. Above just works
# docker compose \
#   --file $BASE/mediator/docker-compose.yml \
#   --project-directory=$BASE/mediator \
#   up --detach

# Start Issuer
$BASE/agent/local/run.sh -n issuer -b -e $BASE/agent/local/.env-issuer -p 8000

# Start Verifier
$BASE/agent/local/run.sh -n verifier -b -e $BASE/agent/local/.env-verifier -p 9000
