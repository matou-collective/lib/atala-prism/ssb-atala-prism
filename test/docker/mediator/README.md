All files copied from: https://github.com/hyperledger/identus-mediator

Start with:
```bash
MEDIATOR_VERSION=0.15.0 docker compose --file ./test/docker/mediator/docker-compose.yml up
```

Get mediatorDID:
```bash
curl --location \
--request GET 'localhost:8080/invitation' \
--header 'Content-Type: application/json'
```

Stop with:
```bash
MEDIATOR_VERSION=0.15.0 docker compose --file ./test/docker/mediator/docker-compose.yml down
```
