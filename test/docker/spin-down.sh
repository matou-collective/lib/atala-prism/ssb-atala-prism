#!/usr/bin/env bash

BASE=./test/docker
source $BASE/set-env.sh

# Stop Mediator
docker compose --file $BASE/mediator/docker-compose.yml down

# Stop Issuer
$BASE/agent/local/stop.sh -n issuer -d

# Stop Verifier
$BASE/agent/local/stop.sh -n verifier -d
