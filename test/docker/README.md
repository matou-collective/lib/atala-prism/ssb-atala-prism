# Docker Test Network Documentation

## Overview

These files facilitate the creation of a local network involving:

- a Mediator
- an Issuer Agent
- a Verifier Agent

## Configuration

The primary configuration file is `set-env.sh` where you can set the versions of the tools. The `docker-compose.yml` files are copied from their respective source directories.

**Default Ports:**

- **Mediator:** 8080
- **Issuer:** 8000
- **Verifier:** 9000

## Running

**To start the test network:**

```bash
./test/docker/spin-up.sh
```

**To see the running processes:**

```bash
docker ps
```

**API Endpoints:**

- **Issuer API Endpoint:** Accessible on port 8000 at [http://localhost:8000/cloud-agent/](http://localhost:8000/cloud-agent/) with a Swagger Interface at [http://localhost:8000/cloud-agent/redoc](http://localhost:8000/cloud-agent/redoc).
- **Verifier API Endpoint:** Accessible on port 9000 at [http://localhost:9000/cloud-agent/](http://localhost:9000/cloud-agent/) with a Swagger Interface at [http://localhost:9000/cloud-agent/redoc](http://localhost:9000/cloud-agent/redoc).

**To stop the test network:**

```bash
./test/docker/spin-down.sh
```
