All files copied from: https://github.com/hyperledger/identus-cloud-agent/blob/main/infrastructure

## Linux

Start with:
```bash
./test/docker/agent/local/run.sh -n issuer -b -e ./test/docker/agent/local/.env-issuer -p 8000 -d 192.168.1.76
```

Shut down Agent:
```bash
./test/docker/agent/local/stop.sh -n issuer -d
```
