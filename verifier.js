const { v4: makeUUID } = require('uuid')

class Verifier {
  constructor (verifierConfig) {
    const { VERIFIER_APIKEY, VERIFIER_URL } = verifierConfig
    this.VERIFIER_APIKEY = VERIFIER_APIKEY
    this.VERIFIER_URL = VERIFIER_URL
    this.VERIFIER_VERSION = '1.39.0'
  }

  async healthCheck () {
    const URL = `${this.VERIFIER_URL}/_system/health`

    try {
      const response = await fetch(URL, {
        method: 'GET',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json'
        }
      })

      const json = await response.json()
      if (!json?.version) throw new Error('Verifier health check failed')
      // if (json?.version !== this.VERIFIER_VERSION) {
      //   throw new Error(`Verifier Version: Your Verifier version is not support v${json.version}. Ahau currently supports v${this.VERIFIER_VERSION}`)
      // }
    } catch (error) {
      if (error.message.match(/fetch failed/)) {
        throw new Error(`Verifier URL: Your Verifier URL failed to fetch at ${this.VERIFIER_URL}. check ${URL}`)
      } else throw error
    }
  }

  // Static async method for initialization
  static async create (verifierConfig) {
    const instance = new Verifier(verifierConfig)
    await instance.healthCheck()
    return instance
  }

  // Instance methods for various operations
  async createConnectionInvitation (opts = {}) {
    try {
      const response = await fetch(`${this.VERIFIER_URL}/connections`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          apiKey: this.VERIFIER_APIKEY
        },
        body: JSON.stringify({
          label: 'Connect Verifier and Member',
          ...opts
        })
      })

      const json = await response.json()

      if (json.status === 403) throw new Error(`Verifier returned an error: ${json.detail}`)
      if (!json.invitation) throw new Error('Verifier didnt return an invitation', { cause: json })

      return json.invitation
    } catch (error) {
      console.error('Verifier: failed to create connection invitation')
      console.error(error)
      throw error
    }
  }

  async createPresentationRequest (connectionId, trustedIssuerDID, triesRemaining = 4) {
    try {
      const response = await fetch(`${this.VERIFIER_URL}/present-proof/presentations`, {
        method: 'POST',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
          apiKey: this.VERIFIER_APIKEY
        },
        body: JSON.stringify({
          connectionId,
          options: {
            challenge: makeUUID(), // random seed prover has to sign to prevent replay attacks
            domain: this.VERIFIER_URL
          },
          proofs: [
            // Add logic to manage proof schemas
            // Array of objects (ProofRequestAux)
            // The type of proofs requested in the context of this proof presentation request (e.g., VC schema, trusted issuers, etc.)
            // {
            //    schemaId: String (required) - The unique identifier of a schema the VC should comply with.
            //    trustIssuers: [String] - One or more issuers that are trusted by the verifier emitting the proof presentation request.
            // }
          ]
        })
      })

      const presentation = await response.json()
      if (!presentation.presentationId && triesRemaining > 0) {
        return this.createPresentationRequest(connectionId, trustedIssuerDID, --triesRemaining)
      }

      if (!presentation.presentationId) {
        throw new Error('Unable to create presentation request', { cause: presentation })
      }

      return presentation
    } catch (error) {
      console.error('Failed to create presentation request:', error)
      throw error
    }
  }

  async requestPresentationInvitation () {
    try {
      const response = await fetch(`${this.VERIFIER_URL}/present-proof/presentations/invitation`, {
        method: 'POST',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
          apiKey: this.VERIFIER_APIKEY
        },
        body: JSON.stringify({
          options: {
            challenge: makeUUID(), // random seed prover has to sign to prevent replay attacks
            domain: this.VERIFIER_URL
          },
          goalCode: 'present-vp',
          goal: 'Request presentation',
          credentialFormat: 'JWT',
          proofs: [
            // Add logic to manage proof schemas
            // Array of objects (ProofRequestAux)
            // The type of proofs requested in the context of this proof presentation request (e.g., VC schema, trusted issuers, etc.)
            // {
            //    schemaId: String (required) - The unique identifier of a schema the VC should comply with.
            //    trustIssuers: [String] - One or more issuers that are trusted by the verifier emitting the proof presentation request.
            // }
          ]
        })
      })

      const presentation = await response.json()

      // TODO: relevant checks could be made here?
      // if (!presentation.presentationId && triesRemaining > 0) {
      //   return this.createPresentationRequest(connectionId, trustedIssuerDID, --triesRemaining)
      // }

      if (presentation.status === 403) throw new Error(`Verifier returned an error: ${presentation.detail}`)
      if (!presentation.presentationId) {
        throw new Error('Unable to request a presentation OOB', { cause: presentation })
      }

      if (!presentation.invitation) throw new Error('Verifier didnt return an invitation', { cause: presentation })

      return presentation
    } catch (error) {
      console.error('Failed to get a presentation invitation', error)
      throw error
    }
  }

  async acceptPresentation (presentationId) {
    try {
      const response = await fetch(`${this.VERIFIER_URL}/present-proof/presentations/${presentationId}`, {
        method: 'PATCH',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
          apiKey: this.VERIFIER_APIKEY
        },
        body: JSON.stringify({
          action: 'presentation-accept'
        })
      })

      return response.json()
    } catch (error) {
      console.error(`Failed to accept presentation: ${presentationId}`, error)
      throw error
    }
  }

  async getPresentation (presentationId) {
    try {
      const response = await fetch(`${this.VERIFIER_URL}/present-proof/presentations/${presentationId}`, {
        method: 'GET',
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
          apikey: this.VERIFIER_APIKEY
        }
      })

      const json = await response.json()

      return json
    } catch (error) {
      console.error(`Failed to get presentation: ${presentationId}`, error)
      throw error
    }
  }
}

module.exports = Verifier

// getPrismDID () {
//   // get issuers PrismDID
//   return fetch(`${VERIFIER_URL}/did-registrar/dids`, {
//     method: 'GET',
//     headers: {
//       accept: 'application/json',
//       'Content-Type': 'application/json',
//       apiKey: VERIFIER_APIKEY
//     }
//   })
//     .then(response => response.json())
//     .then(async (json) => {
//       // NOTE: update to use published DID's
//       return json.contents[0]?.longFormDid ?? await createPrismDID(VERIFIER_URL, VERIFIER_APIKEY)
//     })
// },

// function createPrismDID (ISSUER_URL, ISSUER_APIKEY) {
//   console.log('no dids found. Creating one...')
//   return fetch(`${ISSUER_URL}/did-registrar/dids`, {
//     method: 'POST',
//     headers: {
//       accept: 'application/json',
//       'Content-Type': 'application/json',
//       apiKey: ISSUER_APIKEY
//     },
//     body: JSON.stringify({
//       documentTemplate: {
//         publicKeys: [
//           {
//             id: 'auth-1',
//             purpose: 'authentication'
//           },
//           {
//             id: 'assert-1',
//             purpose: 'assertionMethod'
//           }
//         ],
//         services: []
//       }
//     })
//   })
//     .then(response => response.json())
//     .then(json => {
//       return json.longFormDid
//     })
// }
