# Architecture

This is a `secret-stack` plugin, which allows it to be integrated into, and
access our scuttlebutt stack.

This module has 3 main areas:
1. low-level APIs for performing discrete actions
2. high-level automation around VC issuance/presentation
3. graphql schema to expose relevent queries/mutations in front-end

This module is designed to be used by community members ("Holders" of VCs),
kaitaiki ("Issuers" AND "Verifiers" of VCs). In our context any peer could
simultaneously be all roles at once (though usually for different groups).

## Diagrams

See [Issuance Diagrams](./digrams/issuance.md) and [Presentation Diagrams](./digrams/presentation.md) for a more detailed look at the states between peers through the credential issuing and presenting processes.

## 1. Low-level APIs

see `index.js`

`ssb.atalaPrism.start()` takes care of initializing an atala-prism agent using
the SDK. This is primarily for Holders.


## 2. Hih-level automation

One of the challenges in AtalaPrism is how out-of-band communication is
achieved. In our context peers are already in communication via p2p replication
(over scuttlebutt), so we use this as a messaging layer for many steps.
