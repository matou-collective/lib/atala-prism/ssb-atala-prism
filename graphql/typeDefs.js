const gql = require('graphql-tag')

module.exports = gql`
  scalar CredentialSubject
  scalar MessageAttachment
  scalar IdentusWalletMessages

  extend type Query {
    """
    get all AtalaPrism verifiable credentials
    """
    verifiableCredentials: [JWTVerifiablePayload]

    """
    get all AtalaPrism/ Identus messages
    """
    getPlutoMessages: [IdentusWalletMessages]
  }
  
  extend type Mutation {
    """
    start AtalaPrism agent
    """
    startAtalaPrism: String

    """
    offer an AtalaPrism Verifiable Credential
    """
    offerCredential(tribeId: String!, poBoxId: String!, feedId: String!, claims: MembershipClaimInput!, connectionless: Boolean): String

    """
    request an AtalaPrism Verifiable Credential
    """
    requestPresentation(tribeId: String!, poBoxId: String!, feedId: String!, connectionless: Boolean): String

    """
    accept a connection request
    """
    sendCredentialProof(invitationUrl: String!, credentialId: String!): String
  }

  # see @ssb-graphql/tribes
  extend type GroupApplication {
    credentialPresentations: [VerifiablePresentation]
  }

  type JWTVerifiablePayload implements VerifiableCredential {
    id: String
    verifiableCredential: JWTVerifiableCredential
    iss: String
    sub: String
    jti: String
    nbf: Int
    exp: Int
    aud: [String]
    originalJWTString: String
    credentialType: CredentialType
    context: [String]
    type: [String]
    credentialSchema: VerifiableCredentialTypeContainer
    credentialSubject: CredentialSubject
    credentialStatus: VerifiableCredentialTypeContainer
    refreshService: VerifiableCredentialTypeContainer
    evidence: VerifiableCredentialTypeContainer
    termsOfUse: VerifiableCredentialTypeContainer
    issuer: DID
    subject: DID
    issuanceDate: String
    expirationDate: String
    validFrom: VerifiableCredentialTypeContainer
    validUntil: VerifiableCredentialTypeContainer
    proof: String
  }

  interface VerifiableCredential {
    id: String
    credentialType: CredentialType
    context: [String]
    type: [String]
    credentialSchema: VerifiableCredentialTypeContainer
    credentialSubject: CredentialSubject
    credentialStatus: VerifiableCredentialTypeContainer
    refreshService: VerifiableCredentialTypeContainer
    evidence: VerifiableCredentialTypeContainer
    subject: DID
    termsOfUse: VerifiableCredentialTypeContainer
    issuer: DID
    issuanceDate: String
    expirationDate: String
    validFrom: VerifiableCredentialTypeContainer
    validUntil: VerifiableCredentialTypeContainer
    proof: String
    aud: [String]
  }

  type VerifiableCredentialTypeContainer {
    id: String
    type: String
  }

  type DID {
    schema: String
    method: String
    methodId: ID
  }

  type JWTVerifiableCredential {
    subject: CredentialSubject
    context: [String]
    type: [String]
    credentialSchema: VerifiableCredentialTypeContainer
    credentialSubject: CredentialSubject
    credentialStatus: VerifiableCredentialTypeContainer
    refreshService: VerifiableCredentialTypeContainer
    evidence: VerifiableCredentialTypeContainer
    termsOfUse: VerifiableCredentialTypeContainer
  }
  
  enum CredentialType {
    jwt
    w3c
    Unknown
  }

  type VerifiablePresentation {
    state: String!
    iat: Int
    iss: String
    aud: String
    nonce: String
    credentials: [JWTVerifiablePresentationPayload]
  }

  # NOTE I just made this name up. No idea what to call it?
  type JWTVerifiablePresentationPayload {
    iss: String
    sub: String
    nbf: Int
    issuanceDate: String
    credentialSubject: RegistrationCredentialSubject
    # issKnown: Boolean
    # TODO // add the concept of "known credentials" somewhere, then provide this
  }
  type RegistrationCredentialSubject {
    person: RegistrationCredentialSubjectPerson
    memberOf: RegistrationCredentialSubjectMember
  }
  type RegistrationCredentialSubjectPerson {
    fullName: String!
    dateOfBirth: String
  }
  type RegistrationCredentialSubjectMember {
    tribeId: String!
    tribeName: String
  }


  input MembershipClaimInput {
    person: MembershipPersonInput!
  }
  input MembershipPersonInput {
    fullName: String!
    dateOfBirth: String!
  }
`

// Currently Unused
/*

  extend type Query {
    """
    get all peerDID's from database
    """
    getAllPeerDIDs: [PeerDID]

    """
    get all messages
    """
    getAllMessages: [Message]
  }

  input ConnectionInvitation {
    id: String
    type: String
    from: String
    invitationUrl: String
  }

  type PeerDID {
    did: DID
    # privateKeys:
  }

  type Message {
    from: DID
    to: DID
    thid: String
    piuri: String
    id: String
    body: String
    attachments: [MessageAttachment]
    direction: Int
    expiresTimePlus: String
    extraHeaders: [String]
    createdTime: String
    ack: [String]
    fromPrior: String
    pthid: String
  }

  extend type Mutation {
    """
    accept a connection invitation. return connection success messageId
    """
    acceptConnectionInvitation(input: ConnectionInvitation): String

    """
    accept a credential offer
    """
    acceptCredentialOffer(id: String!): JWTVerifiablePayload
  }

  input MessageInput {
    from: DIDInput
    to: DIDInput
    thid: String
    piuri: String
    id: String
    body: String
    attachments: [MessageAttachment]
    direction: Int
    expiresTimePlus: String
    extraHeaders: [String]
    createdTime: String
    ack: [String]
    fromPrior: String
    pthid: String
  }

  input DIDInput {
    schema: String
    method: String
    methodId: ID
  }

*/
