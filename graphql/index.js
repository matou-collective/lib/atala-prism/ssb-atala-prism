module.exports = function (ssb) {
  return {
    typeDefs: require('./typeDefs'),
    resolvers: require('./resolvers')(ssb)
  }
}
