const { GraphQLError } = require('graphql')
const pull = require('pull-stream')
const jose = require('jose')

const { pullAutoPresentationByRecps } = require('../lib')

module.exports = function (ssb) {
  return {
    Query: {
      verifiableCredentials () {
        return ssb.atalaPrism.getAllCredentials()
          .catch(err => {
            console.log('err with getAllCredentials, falling back to verifiableCredentials', err)
            return ssb.atalaPrism.verifiableCredentials()
          })
        // NOTE I think this works? getAllCredentials uses the local pluto
        // store, so faster?
      },
      getPlutoMessages () {
        return ssb.atalaPrism.getAllMessages()
      }
    },
    Mutation: {
      startAtalaPrism: ssb.atalaPrism.start,

      offerCredential (parent, { tribeId, poBoxId, feedId, claims, connectionless }) {
        return connectionless
          ? ssb.atalaPrism.requestIssuanceInvitation(tribeId, poBoxId, feedId, claims)
            .catch(err => { throw new GraphQLError(err) })
          : ssb.atalaPrism.offerCredential(tribeId, poBoxId, feedId, claims)
            .catch(err => { throw new GraphQLError(err) })
      },

      requestPresentation (parent, { tribeId, poBoxId, feedId, connectionless }) {
        return connectionless
          ? ssb.atalaPrism.requestPresentationInvitation(tribeId, poBoxId, feedId)
            .catch(err => { throw new GraphQLError(err) })
          : ssb.atalaPrism.requestPresentation(tribeId, poBoxId, feedId)
            .catch(err => { throw new GraphQLError(err) })
      },

      sendCredentialProof (parent, { invitationUrl, credentialId }) {
        return ssb.atalaPrism.sendCredentialProof(invitationUrl, credentialId)
          .catch(err => { throw new GraphQLError(err) })
      }
    },

    JWTVerifiablePayload: {
      // PATCH - see https://github.com/input-output-hk/atala-prism-wallet-sdk-ts/pull/175
      // TODO - revisit this after PR / bug is resolved
      issuanceDate (payload) {
        let nbfValue = payload.properties.get('nbf')
        if (nbfValue < 2e10) nbfValue = nbfValue * 1000
        return new Date(nbfValue).toISOString()
      }
    },
    JWTVerifiablePresentationPayload: {
      // PATCH - see https://github.com/input-output-hk/atala-prism-wallet-sdk-ts/pull/175
      // TODO - revisit this after PR / bug is resolved
      issuanceDate (payload) {
        let nbfValue = payload.nbf
        if (nbfValue < 2e10) nbfValue = nbfValue * 1000
        return new Date(nbfValue).toISOString()
      }
    },

    GroupApplication: {
      async credentialPresentations (parent) {
        const recps = parent.recps
        // feedId should be the second element?

        // look up a presentation from the applicant
        return pull(
          pullAutoPresentationByRecps(ssb, recps),

          // accepted presentations are stored as nested JWT
          // pull.filter(record => record.JWT),
          pull.map(record => {
            if (!record.JWT) {
              return {
                state: record.state
              }
            }

            const presentation = jose.decodeJwt(record.JWT)
            const credentials = presentation.vp.verifiableCredential
              .map(c => {
                const credential = jose.decodeJwt(c)
                credential.credentialSubject = credential.vc.credentialSubject
                return credential
              })
            return {
              state: record.state,
              ...presentation,
              credentials
            }
          }),

          pull.take(1),
          pull.collectAsPromise()
        )
      }
    }
  }
}

/*
 we _could_ require less args from grqphql query
 - drop poBoxId, look it up via tribeId
 - drop claims, derive it from the profile
*/

// async function getPOBoxId (tribeId) {
//   return p(ssb.tribes.findSubGroupLinks)(tribeId)
//     .then(links => {
//       const adminGroupLink = links.find(link => link.admin)
//       if (!adminGroupLink) throw new Error('cannot find kaitiaki subgroup')

//       return adminGroupLink.subGroupId
//     })
//     .then(subGroupId => p(ssb.tribes.poBox.get)(subGroupId))
//     .then(info => info.poBoxId)
// }

// async function getProfileForClaim (tribeId, feedId) {
//   return p(ssb.profile.findByFeedId)(feedId, { groupId: tribeId })
//     .then(profiles => {
//       console.log(profiles)
//     })
// }
