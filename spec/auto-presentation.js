const Overwrite = require('@tangle/overwrite')

// when an application comes in to join our tribe
// any kaitiaki can kick off a "auto-request-presentation" process
//   1. kaitiaki sends the applicant a connection request (oob)
//   2. the applicant auto-replies (connection complete)
//   3. the kaitiaki sees the connection is "go" and sends a presentatin request
//   4. the application auto-replies with presentation
//   5. the kaitiaki sees presentation(s) in the UI

// this arc assumes the kaitiaki needs to be present to progress the Verifier
// state
//
// what if, the verification could be started without the kaitiaki? - verifier
// agent always online - applicant would need API_KEY - or an endpoint that
// allowed them to hit it?
//
// TODO: figure out if we can jump this queue of state faster to make more
// reliable presentation (because waiting for the kaitiaki to be online to start
// the connection, then request presentation might be quite slow)

// states
const PRES_CONN_INVITE_SENT = 'presentation connection invite sent'
const PRES_INVITE_SENT = 'presentation invite sent'

const PRES_CONN_COMPLETE = 'presentation connection complete'
const PRES_REQUEST_SENT = 'presentation request sent'

const PRES_SENT = 'presentation sent'
const PRES_COMPLETE = 'presentation complete'

// WATCH we may want to seperate out connection + credential tracking records
const spec = {
  type: 'atalaPrism/auto-presentation',
  staticProps: {
    tribeId: { type: 'string' },
    // new-member-id
    //   feedId + tribeId => personGroupProfile => legalName + DOB
    //   groupProfileId => legalName + DOB
    connection: {
      type: 'object',
      properties: {
        id: { type: 'string' },
        oob: { type: 'string' }
      },
      required: [
        'id', // NOTE - required for original flow
        'oob'
      ]
    }
  },
  props: {
    state: Overwrite({
      enum: [
        // Original
        PRES_CONN_INVITE_SENT,
        PRES_CONN_COMPLETE,
        PRES_REQUEST_SENT,
        // Connectionless
        PRES_INVITE_SENT,
        // Original & Connectionless
        PRES_SENT,
        PRES_COMPLETE
      ]
    }),
    presentationId: Overwrite({
      type: 'string'
    }),
    // the presentation in JWT format (has another JWT with credential inside it)
    JWT: Overwrite({
      type: 'string'
    })
  }
  // TODO state transitions
  //
  // - Original:
  //   1. PRES_CONN_INVITE_SENT
  //   2. PRES_CONN_COMPLETE
  //   3. PRES_REQUEST_SENT
  //   4. PRES_SENT
  //   5. PRES_COMPLETE
  //
  // - Connectionless
  //   1. PRES_INVITE_SENT
  //   2. PRES_SENT
  //   3. PRES_COMPLETE
}

spec.states = {
  PRES_CONN_INVITE_SENT,
  PRES_CONN_COMPLETE,
  PRES_REQUEST_SENT,

  PRES_INVITE_SENT,

  PRES_SENT,
  PRES_COMPLETE
}
spec.state = spec.states

module.exports = spec
