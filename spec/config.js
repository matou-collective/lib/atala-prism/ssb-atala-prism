const DIDSchema = {
  type: 'string',
  pattern: '^did:peer:[a-zA-Z0-9\\.]+$'
}

const URLSchema = {
  type: 'string',
  pattern: '^https?:\\/\\/[^\\s/$.?#].[^\\s]*$'
}

const KeySchema = {
  type: ['string', null],
  pattern: '^[a-zA-Z0-9.\\+_\\-]*$'
}

module.exports = {
  title: 'ssb.config.atalaPrism schema',

  type: 'object',
  properties: {
    mediatorDID: DIDSchema,
    issuers: {
      type: 'object',
      patternProperties: {
        '^%.+cloaked$': {
          type: 'object',
          properties: {
            tribeName: { type: 'string' },
            ISSUER_URL: URLSchema,
            ISSUER_APIKEY: KeySchema
          },
          required: ['tribeName', 'ISSUER_URL'],
          additionalProperties: false
        }
      },
      additionalProperties: false
    },
    verifiers: {
      type: 'object',
      patternProperties: {
        '^%.+cloaked$': {
          type: 'object',
          properties: {
            tribeName: { type: 'string' },
            VERIFIER_URL: URLSchema,
            VERIFIER_APIKEY: KeySchema
          },
          required: ['tribeName', 'VERIFIER_URL'],
          additionalProperties: false
        }
      }
    }
  },
  required: ['mediatorDID'],
  additionalProperties: false
}
