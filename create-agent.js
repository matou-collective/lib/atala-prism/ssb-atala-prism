const { mkdirSync, existsSync } = require('fs')
const SDK = require('@hyperledger/identus-edge-agent-sdk')
const { createLevelDBStorage } = require('@pluto-encrypted/leveldb')

// === HACK =======================================
const nodeCrypto = require('crypto')

Object.defineProperty(globalThis, 'crypto', {
  value: {
    getRandomValues: (arr) => nodeCrypto.getRandomValues(arr)
  }
})
// ================================================

module.exports = async function createAgent (plutoStorePath, password, mediatorDIDString) {
  const mediatorDID = SDK.Domain.DID.fromString(mediatorDIDString)

  // create the directory if its not there
  if (!existsSync(plutoStorePath)) {
    mkdirSync(plutoStorePath, { recursive: true })
  }

  if (!isPassword(password)) {
    throw new Error('password expected')
  }

  const store = new SDK.Store({
    // HACK: name cannot be the same for multiple agents on same computer maybe?
    // so have slapped that unique path on the back there for now
    name: ('ahau' + plutoStorePath),

    storage: createLevelDBStorage({ dbPath: plutoStorePath }),
    password: Buffer.from(password).toString('hex'),

    multiInstance: false,
    ignoreDuplicate: true
  })

  const apollo = new SDK.Apollo()
  const pluto = new SDK.Pluto(
    store,
    apollo
  )

  return SDK.Agent.initialize({ mediatorDID, pluto, apollo })
}

function isPassword (str) {
  return (
    typeof str === 'string' &&
    str.length >= 8
  )
}
