# ssb-atala-prism

This module integrates **@atala/prism-wallet-sdk-ts** functionality with **ahau**.

## Setup

1. **Review `.npmrc.template`**: Make sure to review this file for any specific configurations needed. *(TODO: Add details)*.

2. **Install Dependencies**:

   ```bash
   npm install
   ```

3. **Set Up the Local Test Network**: Follow the instructions in [this README](./test/docker/README.md) to set up the local test network.
    - If set up correctly, you can the following to start up the test network:

      ```bash
      npm run test:setup
      ```

4. **Set Up Environment Variables**:

   - **Create and Configure the `.env` File**:
     Copy the `.env.template` to `.env` and fill in the necessary environment variables for your setup:

     ```bash
     cp .env.template .env
     ```

     Edit the `.env` file to include the appropriate URLs and API keys for your local or production environment:

     ```env
     ISSUER_URL=http://localhost:8000/cloud-agent
     ISSUER_APIKEY=your-issuer-api-key
     VERIFIER_URL=http://localhost:9000/cloud-agent
     VERIFIER_APIKEY=your-verifier-api-key
     MEDIATOR_URL=http://localhost:8080
     ```

   - You can also set up a `.env.production` file and use `npm run test:prod`to test on production cloud agents and mediator.

5. **Run Tests**:

   ```bash
   npm test
   ```

   For debug logging:

   ```bash
   DEBUG=ssb-atala* npm test
   ```

   For interactive debugging:

   ```bash
   node --inspect-brk test/start.test.js
   ```

   Then open `chrome://inspect` in a Chromium/Chrome browser.

## API

### `ssb.atalaPrism.start(cb)`

Starts the AtalaPrism Agent.

### `ssb.atalaPrism.offerCredential(tribeId, poBoxId, feedId, claims, cb)`

Initiates the process of offering a verifiable credential (VC) to a member of a tribe.

- Requires `ISSUER_URL` and `ISSUER_APIKEY` for your tribe (see Config below).
- `claims` is an object with the following structure:

  ```js
  {
    person: {
      fullName: String,
      dateOfBirth: String
    }
  }
  ```

- `cb` is a callback function of form `cb(err)`. If no callback is provided, this method returns a promise.

### `ssb.atalaPrism.verifiableCredentials(cb)`

Retrieves a list of verifiable credentials.

- `cb` is a callback function of form `cb(err, [VC])`. If no callback is provided, this method returns a promise.
- `[VC]` is an array of verifiable credential objects.

### `ssb.atalaPrism.requestPresentation(tribeId, poBoxId, feedId, cb)`

Requests the presentation of an existing VC to the tribe.

- Requires `VERIFIER_URL` and `VERIFIER_APIKEY` for your tribe (see Config below).
- `cb` is a callback function of form `cb(err)`. If no callback is provided, this method returns a promise.

### `ssb.atalaPrism.sendCredentialProof(invitationUrl, credentialId, cb)`

Sends a credential proof to a verifier.

- `invitationUrl` is an out-of-band (oob) connection invite URL provided by a verifier.
- `credentialId` is the ID of the credential you wish to present.
- `cb` (optional) is a callback function. If not provided, this method returns a promise.

## Connectionless API

### `ssb.atalaPrism.requestIssuanceInvitation(tribeId, poBoxId, feedId, claims, cb)`

Sends a request message to the Issuer Agent for a connectionless invitation to issue a credential.

- Requires `ISSUER_URL`and `ISSUER_APIKEY`for your tribe (see Config below).
- `cb` is a callback function of form `cb(err, recordId)`. If no callback is provided, this method returns a promise.

### `ssb.atalaPrism.requestPresentationInvitation(tribeId, poBoxId, feedId, cb)`

Sends a request message to the Verifier Agent for a connectionless invitation to present a credential.

- Requires `VERIFIER_URL` and `VERIFIER_APIKEY` for your tribe (see Config below).
- `cb` is a callback function of form `cb(err, recordId)`. If no callback is provided, this method returns a promise.

### `ssb.atalaPrism.acceptIssuanceInvitation(invitationURL, opts, cb)`

Takes the given invitationURL, parses it and calls the relevant SDK agent.acceptInvitation method to accept the invitation. It then waits for a response message, which is expected to be the credential offer.

- `invitationUrl` is an out-of-band (oob) invite URL provided by an Issuer Agent, for a connectionless issuance of credentials.
- `cb` (optional) is a callback function. If not provided, this method returns a promise.

### `ssb.atalaPrism.acceptPresentationInvitation(invitationURL, opts, cb)`

Takes the given invitationURL, parses it and calls the relevant SDK agent.acceptInvitation method to accept the invitation. It then waits for a response message, which is expected to be the presentation request.

- `invitationUrl` is an out-of-band (oob) invite URL provided by an Issuer Agent, for a connectionless presentation of credentials.
- `cb` (optional) is a callback function. If not provided, this method returns a promise.

## GraphQL

Associated GraphQL endpoints are available for each of the above methods. Refer to `graphql/typeDefs.js` or `test/graphql` for more details.

## Config

To support issuing or verifying credentials, add the following to your `ssb` config:

```json
{
  "atalaPrism": {
    "mediatorDID": "did:peer:2.Ez6LSghwS....",
    "issuers": {
      "[tribeId]": {
        "tribeName": "Whangaroa Papa Hapū",
        "ISSUER_URL": "https://issuer.wph.co.nz/prism-agent",
        "ISSUER_APIKEY": "sdskj087123lekmqawslkdj10p23ulkw1qj3e"
      }
    },
    "verifiers": {
      "[tribeId]": {
        "VERIFIER_URL": "https://verifier.ahau.io/prism-agent",
        "VERIFIER_APIKEY": "k1239asdm1l87sdaj1lkj2387sdjm1lk23m"
      }
    }
  }
}
```

### Config Fields

- `mediatorDID` (String, required): The DID of a mediator responsible for relaying messages between the Atala Prism agents and holders.
- `issuers` (Object, required): Details for each `tribeId` wanting to issue verified credentials.
  - `tribeId` (String, required): The ID for the tribe.
  - `tribeName` (String): The name of the tribe.
  - `ISSUER_URL` (String): The address where the issuer Atala Prism agent is running.
  - `ISSUER_APIKEY` (String): The issuer's API key.
- `verifiers` (Object, required): Details for each `tribeId` wanting to verify credentials.
  - `tribeId` (String, required): The encryption key for the tribe.
  - `VERIFIER_URL` (String): The address where the verifier Atala Prism agent is running.
  - `VERIFIER_APIKEY` (String): The verifier's API key.

### Notes

- Any tribe can offer issuance and/or verification (no requirement to have both).

## Current Issues

- `acceptDIDCommInvitation` doesn't always work reliably. Added logic checks for the connection confirmation message, and if confirmation is not achieved, repeats the `acceptInvitation` process.
- Occasionally, the agent experiences a startup error.

## Development

### 1. Set Up Local Agent Instances for Development and Testing

- Mediator
- Issuer Agent
- Verifier Agent

![Architecture Diagram](./test_architecture.png)

You can set these up:

- **Automatically:** [Setting up local instances using Docker](./test/docker/README.md)
- **Manually:** Refer to the [AtalaPrism Quick Start](https://docs.atalaprism.io/docs/quick-start/) guide.

### 2. Environment Variables

Set up the environment variables by copying the template file and filling in the URLs and keys for these nodes:

```bash
cp .env.template .env
```

Edit the `.env` file to specify your local or production URLs and API keys.

### 3. Install Dependencies and Run Tests

```bash
npm install
npm test
```

For debug logging:

```bash
DEBUG=ssb-atala* npm test
```

For interactive debugging, run:

```bash
node --inspect-brk test/start.test.js
```

Then open `chrome://inspect` in a Chromium/Chrome browser.

### Additional Resources

- [`ARCHITECTURE.md`](ARCHITECTURE.md): Overview of the module's design.
- [`TODO.md`](TODO.md): Track remaining tasks.

---
