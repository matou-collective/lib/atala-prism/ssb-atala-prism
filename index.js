const SDK = require('@hyperledger/identus-edge-agent-sdk')
const CRUT = require('ssb-crut')
const { promisify: p } = require('util')
const c = require('callbackify') // TODO replace with util
const path = require('path')
const debug = require('debug')

const createAgent = require('./create-agent')
const autoRespond = require('./auto-respond')
const Issuer = require('./issuer')
const Verifier = require('./verifier')
const autoIssuanceSpec = require('./spec/auto-issuance')
const autoPresentationSpec = require('./spec/auto-presentation')

const isValidConfig = require('./is-my-config-valid')

const debugConnection = debug('ssb-atala-prism:connection')
const debugCredential = debug('ssb-atala-prism:credential')
const debugPres = debug('ssb-atala-prism:presentation-invitation')
const debugSendProof = debug('ssb-atala-prism:sendProof')

const TIMEOUT_FAILURE = 'timeout failure'
const MIN_RUN_TIME = 3000

function handleSendMessageError (err) {
  if (err.message === 'Malformed: Message is not a valid JWE, JWS or JWM') {
    // ignore DIDCommMalformed error. Its a lie
  } else throw err
}

module.exports = {
  name: 'atalaPrism',

  manifest: {
    start: 'async',

    // PRESENTATION
    // - with connections
    requestPresentation: 'async', // graphql
    acceptPresentationRequest: 'async',
    // - connectionless
    requestPresentationInvitation: 'async',
    acceptPresentationInvitation: 'async',
    // - other
    sendCredentialProof: 'async', // graphql
    getPresentationRequests: 'async',

    // ISSUANCE
    // - with connections
    offerCredential: 'async', // graphql
    // - connectionless
    requestIssuanceInvitation: 'async',
    acceptIssuanceInvitation: 'async',
    // - other
    acceptCredentialOffer: 'async',
    verifiableCredentials: 'async', // graphql

    // HELPERS
    acceptConnectionInvitation: 'async',
    getAllPeerDIDs: 'async',
    getAllPrismDIDs: 'async',
    createNewPrismDID: 'async',
    getAllMessages: 'async',
    awaitMessages: 'async',
    parseOOBInvitation: 'async'
  },

  init (ssb, config) {
    let agent
    let agentStartTime
    const crut = {
      issuance: new CRUT(ssb, autoIssuanceSpec),
      presentation: new CRUT(ssb, autoPresentationSpec)
    }

    // shut down atalaPrism agent on ssb close
    ssb.close.hook(async function (close, args) {
      if (agent && agent.stop) {
        // ensure agent has been running for at least MIN_RUN_TIME
        const wait = Math.max(
          0,
          MIN_RUN_TIME - (Date.now() - agentStartTime)
        )

        await agent.stop()
        await p(setTimeout)(wait)
      }
      close.apply(this, args)
    })

    // start edge agent
    async function start (cb) {
      if (cb === undefined) return p(start)()
      if (agent) return cb(null, agent)

      if (!isValidConfig(config.atalaPrism)) {
        const error = new Error(
          isValidConfig.errorString,
          { cause: isValidConfig.errors }
        )
        return cb(error)
      }

      const plutoStorePath = path.join(config.path, 'atalaPrism')
      const password = 'pluto-' + config.keys.secret

      createAgent(plutoStorePath, password, config.atalaPrism.mediatorDID)
        .then(_agent => {
          agentStartTime = Date.now()
          agent = _agent
          return agent.start()
        })
        .then(() => {
          autoRespond(ssb, config, crut)

          cb(null, agent)
        })
        .catch(err => {
          // console.log(err)
          cb(err)
        })
    }

    // message listener helper function
    function awaitMessageResponse (thid, piuri) {
      let handleMessage
      const promise = new Promise((resolve, reject) => {
        handleMessage = (messages) => {
          const message = messages.find(m => (
            m.thid === thid &&
            m.piuri === piuri
          ))
          if (!message) return

          agent.removeListener('message', handleMessage)
          resolve(message)
        }

        agent.addListener('message', handleMessage)
      })

      promise.cancel = () => {
        agent.removeListener('message', handleMessage)
      }

      return promise
    }

    // wait 'time' seconds for connection confirmation, else fail
    function awaitMessageResponseForTime (thid, piuri, time) {
      let timer
      const query = awaitMessageResponse(thid, piuri)

      return Promise.race([
        query,
        new Promise((resolve, reject) => {
          timer = setTimeout(resolve, time, TIMEOUT_FAILURE)
        })
        // TODO change this to reject, and then catch and process the error in promise-chain
        // need to check how Promise.race behaves with reject tho!
      ])
        .then(async res => {
          if (res !== TIMEOUT_FAILURE) return res

          // HACK - atala agent / pluto bug
          // sometimes the listener does not work, BUT the message is in pluto (T_T
          const msgs = await ssb.atalaPrism.getAllMessages()
          const msg = msgs.find(m => (
            m.thid === thid &&
            m.piuri === piuri
          ))
          if (!msg) return TIMEOUT_FAILURE
          console.log('FALLBACK - message listener failed, used pluto')
          return msg
        })
        .finally(() => {
          clearTimeout(timer)
          query.cancel()
        })
    }

    async function parseOOBInvitation (invitationURL) {
      const url = typeof invitationURL === 'string' ? new URL(invitationURL) : invitationURL
      return agent.parseOOBInvitation(url)
    }

    // This function
    // - gets the confirmation message (starts a listener which waits 10 seconds)
    // - if that doesn't work, then starts the listener again
    // - if it does this `tries` times, then it fails
    async function acceptConnectionInvitation (invitationURL, { triesRemaining = 5, agentName }, cb) {
      if (cb === undefined) return p(acceptConnectionInvitation)(invitationURL, { triesRemaining, agentName })
      debugConnection('start: accept connection invitation process (' + triesRemaining + ')')

      debugConnection('  1: process OOB url into an invitation')
      const oobInvitation = await parseOOBInvitation(invitationURL)
      const thid = oobInvitation.id

      debugConnection('  2: accept the connection invitation')
      return agent.acceptDIDCommInvitation(oobInvitation)
        .then(() => {
          debugConnection(`  3: wait for connection confirmation response from ${agentName}`)
          return awaitMessageResponseForTime(
            thid,
            'https://atalaprism.io/mercury/connections/1.0/response',
            10000
          )
        })
        .then(res => {
          if (triesRemaining === 0) throw new Error('Error trying to establish connection')
          if (res === TIMEOUT_FAILURE) {
            triesRemaining--
            // URGENT: @atalaPrism: why does this sometimes fail??
            debugConnection('  4: accepting connection TIMEOUT_FAILURE....i dont know why..., trying again')
            return acceptConnectionInvitation(invitationURL, { agentName, triesRemaining }, cb)
          }
          cb(null, res.id)
        })
        .catch(cb)
    }

    // function handles accepting a connectionless presentation invitation
    // and waits for the confirmation message which is a presentation request
    async function acceptPresentationInvitation (invitationURL, { triesRemaining = 5, agentName }, cb) {
      if (cb === undefined) return p(acceptPresentationInvitation)(invitationURL, { triesRemaining, agentName })
      debugPres('start: accept invitation process (' + triesRemaining + ')')

      debugPres('  1: process OOB url into an invitation')
      const oobInvitation = await parseOOBInvitation(invitationURL)
      const thid = oobInvitation.id

      debugPres('  2: accept the invitation')
      return agent.acceptInvitation(oobInvitation)
        .then(() => {
          debugPres(`  3: wait for presentation request from the ${agentName}`)
          return awaitMessageResponseForTime(
            thid,
            'https://didcomm.atalaprism.io/present-proof/3.0/request-presentation',
            10000
          )
        })
        .then(res => {
          if (triesRemaining === 0) throw new Error('Error trying to accept presentation invitation')
          if (res === TIMEOUT_FAILURE) {
            triesRemaining--
            // URGENT: @atalaPrism: why does this sometimes fail??
            debugPres('  4: accepting invitation TIMEOUT_FAILURE....i dont know why..., trying again')
            return acceptPresentationInvitation(invitationURL, { agentName, triesRemaining }, cb)
          }
          cb(null, res.id)
        })
        .catch(cb)
    }

    // function handles accepting a connectionless presentation invitation
    // and waits for the confirmation message which is a presentation request
    async function acceptIssuanceInvitation (invitationURL, { triesRemaining = 5, agentName }, cb) {
      if (cb === undefined) return p(acceptIssuanceInvitation)(invitationURL, { triesRemaining, agentName })
      debugCredential('start: accept invitation process (' + triesRemaining + ')')

      debugCredential('  1: process OOB url into an invitation')
      const oobInvitation = await parseOOBInvitation(invitationURL)
      const thid = oobInvitation.id

      debugCredential('  2: accept the invitation')
      // TODO; hyperledger docs say acceotDIDCommInvitation for issuance but acceptInvitation for presentation
      // return agent.acceptDIDCommInvitation(oobInvitation)
      return agent.acceptInvitation(oobInvitation)
        .then(() => {
          debugCredential(`  3: wait for issuance from the ${agentName}`)
          return awaitMessageResponseForTime(
            thid,
            'https://didcomm.org/issue-credential/3.0/offer-credential',
            10000
          )
        })
        .then(res => {
          if (triesRemaining === 0) throw new Error('Error trying to accept issuance invitation')
          if (res === TIMEOUT_FAILURE) {
            triesRemaining--
            // URGENT: @atalaPrism: why does this sometimes fail??
            debugCredential('  4: accepting invitation TIMEOUT_FAILURE....i dont know why..., trying again')
            return acceptIssuanceInvitation(invitationURL, { agentName, triesRemaining }, cb)
          }
          cb(null, res.id)
        })
        .catch(cb)
    }

    async function acceptCredentialOffer (offerMessage, cb) {
      if (cb === undefined) return p(acceptCredentialOffer)(offerMessage)
      debugCredential('start: accept credential offer process')

      debugCredential('  1: process the "offerMessage" into a "OfferCredential"')
      const offerCredential = await SDK.OfferCredential.fromMessage(offerMessage)

      debugCredential('  2: creates a credential request from the "OfferCredential"')
      agent.prepareRequestCredentialWithIssuer(offerCredential)
        .then(async requestCredential => {
          debugCredential('  3: put the request into a message')
          const message = requestCredential.makeMessage()

          // start listening for a response
          // NOTE we don't await here, because we want to sendMessage after this.
          // we await this query later
          const query = awaitMessageResponseForTime(
            message.thid,
            'https://didcomm.org/issue-credential/3.0/issue-credential',
            60000
          )

          debugCredential('  4: send the message to issuer')
          agent.sendMessage(message).catch(handleSendMessageError)

          debugCredential('  5: wait for the credential to be auto-issued')
          const response = await query
          if (response !== TIMEOUT_FAILURE) return response
          else throw new Error('Failed to get confirmation message')
        })
        .then(credentialMessage => {
          // if (credentialMessage === TIMEOUT_FAILURE) return
          debugCredential('  6: processes the credential message into an "IssuedCredential"')
          const issueCredential = SDK.IssueCredential.fromMessage(credentialMessage)

          debugCredential('  7: processes the IssuedCredential and save it to the DB')
          return agent.processIssuedCredentialMessage(issueCredential)
        })
        .then(res => cb(null, res))
        .catch(cb)
    }

    // TODO callbackify
    function getPresentationRequests (cb) {
      if (cb === undefined) return p(getPresentationRequests)()

      // TODO: get message by id
      return agent.pluto.getAllMessages()
        .then(res => res.filter(m => m.piuri === 'https://didcomm.atalaprism.io/present-proof/3.0/request-presentation'))
        .then(res => cb(null, res))
        .catch(cb)
    }

    // TODO callbackify
    function acceptPresentationRequest (request, credential, cb) {
      if (cb === undefined) return p(acceptPresentationRequest)(request, credential)

      const requestPresentationMessage = SDK.RequestPresentation.fromMessage(request)
      return agent.createPresentationForRequestProof(requestPresentationMessage, credential)
        .then(presentation => {
          const message = presentation.makeMessage()

          // const response = getConfirmationMessage('https://didcomm.org/present-proof/3.0/request-presentation', 20000)

          return agent.sendMessage(message).catch(handleSendMessageError)
        })
        .then(res => cb(null, res))
        .catch(cb)
    }

    // TODO callbackify
    function offerCredential (tribeId, poBoxId, feedId, claims, cb) {
      if (cb === undefined) return p(offerCredential)(tribeId, poBoxId, feedId, claims)

      if (claims.tribe) return cb(Error('cannot inject claims.tribe'))

      const issuerConfig = config.atalaPrism?.issuers?.[tribeId]
      if (!issuerConfig) return cb(Error('No issuer config found for ' + tribeId))

      Issuer.create(issuerConfig)
        .then(issuer => issuer.createConnectionInvitation())
        .then(invitation => {
          return crut.issuance.create({
            tribeId,
            claims: {
              memberOf: {
                tribeId,
                tribeName: issuerConfig.tribeName
              },
              ...claims
            },
            connection: {
              id: invitation.id,
              oob: invitation.invitationUrl
            },
            state: autoIssuanceSpec.states.CRED_CONN_INVITE_SENT,
            recps: [poBoxId, feedId]
          })
        })
        .then(msgId => cb(null, msgId))
        .catch(cb)
    }
    // WIP
    // - [ ] find difference between
    //    - getAllCredentials
    //    - verifiableCredentials

    function requestPresentation (tribeId, poBoxId, feedId, cb) {
      // this kicks of the auto-respond chain for presentation
      // could consider renaming "start presentation process"
      if (cb === undefined) return p(requestPresentation)(tribeId, poBoxId, feedId)

      const verifierConfig = config.atalaPrism?.verifiers?.[tribeId]
      if (!verifierConfig) return cb(Error('No verifier config found for ' + tribeId))

      Verifier.create(verifierConfig)
        .then(verifier => verifier.createConnectionInvitation())
        .then(invitation => {
          return crut.presentation.create({
            tribeId,
            connection: {
              id: invitation.id,
              oob: invitation.invitationUrl
            },
            state: autoPresentationSpec.state.PRES_CONN_INVITE_SENT,
            recps: [poBoxId, feedId]
          })
        })
        .then(msgId => cb(null, msgId))
        .catch(cb)
    }

    function requestPresentationInvitation (tribeId, poBoxId, feedId, cb) {
      if (cb === undefined) return p(requestPresentationInvitation)(tribeId, poBoxId, feedId)

      const verifierConfig = config.atalaPrism?.verifiers?.[tribeId]
      if (!verifierConfig) return cb(Error('No verifier config found for ' + tribeId))

      Verifier.create(verifierConfig)
        .then(verifier => verifier.requestPresentationInvitation())
        .then(presentation => {
          const { presentationId, invitation } = presentation
          return crut.presentation.create({
            tribeId,
            presentationId,
            connection: {
              id: invitation.id,
              oob: invitation.invitationUrl
            },
            state: autoPresentationSpec.state.PRES_INVITE_SENT,
            recps: [poBoxId, feedId]
          })
        })
        .then(msgId => cb(null, msgId))
        .catch(cb)
    }

    function requestIssuanceInvitation (tribeId, poBoxId, feedId, claims, cb) {
      if (cb === undefined) return p(requestIssuanceInvitation)(tribeId, poBoxId, feedId, claims)

      const issuerConfig = config.atalaPrism?.issuers?.[tribeId]
      if (!issuerConfig) return cb(Error('No issuer config found for ' + tribeId))

      claims = {
        memberOf: {
          tribeId,
          tribeName: issuerConfig.tribeName
        },
        ...claims
      }

      Issuer.create(issuerConfig)
        .then(async issuer => {
          const DID = await issuer.getPrismDID()
          return issuer.requestIssuanceInvitation(DID, claims)
        })
        .then(res => {
          const { invitation } = res

          return crut.issuance.create({
            tribeId,
            claims,
            connection: {
              id: invitation.id,
              oob: invitation.invitationUrl
            },
            state: autoIssuanceSpec.states.CRED_INVITE_SENT,
            offerThid: res.thid,
            recps: [poBoxId, feedId]
          })
        })
        .then(msgId => cb(null, msgId))
        .catch(cb)
    }

    function sendCredentialProof (invitationUrl, credentialId, cb) {
      if (cb === undefined) return p(sendCredentialProof)(invitationUrl, credentialId)
      debugSendProof('start: send credential proof')

      const invite = extractInviteFromInvitationUrl(invitationUrl)
      if (!invite) return cb(new Error('invalid invitiationUrl', { cause: invitationUrl }))

      // wait for request from Verifier
      agent.addListener('message', handlePresentationRequest)

      async function handlePresentationRequest (messages) {
        const message = messages.find(message => (
          message.piuri === 'https://didcomm.atalaprism.io/present-proof/3.0/request-presentation' &&
          message.from.toString() === invite.from
        ))
        if (!message) return

        debugSendProof('messageListner: found presentation message from invite')
        // get credential
        const credential = await agent.pluto.getAllCredentials()
          .then(credentials => {
            if (!Array.isArray(credentials)) return credentials
            return credentials.find(cred => cred.id === credentialId)
          })

        if (!credential) return cb(new Error('Error finding credential'))

        try {
          debugSendProof('messageListner: got credential from credential.id')
          // accept presentation request
          await acceptPresentationRequest(message, credential)

          cb(null, 'request accepted')
        } catch (err) {
          cb(err)
        }

        agent.removeListener('message', handlePresentationRequest)
      }

      // kick off the connection accept
      // => verifier accepts connection, then issues presentation request
      // (webhook: verifier pushes update to server, which can then progress
      // to sending presentation request)
      acceptConnectionInvitation(invitationUrl, { agentName: 'verifier' })
        .catch(err => cb(new Error(err, { cause: 'sendCredentialProof failed at acceptConnectionInvitation' })))
    }

    return {
      start,

      // PRESENTATION
      // - with connections
      requestPresentation,
      acceptPresentationRequest,
      // - connectionless
      requestPresentationInvitation,
      acceptPresentationInvitation,
      // - other
      sendCredentialProof,
      getPresentationRequests,

      // ISSUANCE
      // - with connections
      offerCredential,
      // - connectionless
      requestIssuanceInvitation,
      acceptIssuanceInvitation,
      // - other
      acceptCredentialOffer,
      verifiableCredentials: c(() => agent.verifiableCredentials()),

      // HELPERS
      acceptConnectionInvitation,
      getAllPeerDIDs: c(() => agent.pluto.getAllPeerDIDs()),
      getAllPrismDIDs: c(() => agent.pluto.getAllPrismDIDs()),
      createNewPrismDID: c((alias, service) => agent.createNewPrismDID(alias, service)),
      getAllCredentials: c(() => agent.pluto.getAllCredentials()),
      getAllMessages: c(() => agent.pluto.getAllMessages()),
      awaitMessages: c(() => agent.manager.awaitMessages()),

      // for testing only
      addListener: (type, cb) => agent.addListener(type, cb),
      awaitMessageResponseForTime,
      parseOOBInvitation
    }
    // c = callbackify
    // Note
    //   - callbackify turns a promise into a callback
    //   - if you don't pass a callback, it falls back to Promise mode!
    //   - we don't have agent straight off, so we have to write it ugly...
  }
}

function extractInviteFromInvitationUrl (oobInvitationUrl) {
  const code = oobInvitationUrl.split('oob=')[1]
  if (!code) return

  try {
    return JSON.parse(Buffer.from(code, 'base64').toString())
  } catch (err) {
    console.log(err)
  }
}
